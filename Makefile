# Makefile for Othello
# Compiles the code for the game
# Created on 11/11/2017
# Created by Andrew Davis
#
# Copyright (C) 2017  Andrew Davis
#
# This program is free software: you can redistribute it and/or modify   
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# define the compiler
CXX=g++

# define the compiler flags
CXXFLAGS=-c -Wall -std=c++14

# define the linker flags
LDFLAGS=-lcppcurses -lm

# define state-specific compiler flags
debug: CXXFLAGS += -g

# retrieve the source code
MAIN=$(shell ls src/*.cpp)
CORE=$(shell ls src/core/*.cpp)
PIECE=$(shell ls src/piece/*.cpp)
UTIL=$(shell ls src/util/*.cpp)
EXCEPT=$(shell ls src/except/*.cpp)
BOARD=$(shell ls src/board/*.cpp)
CLXN=$(shell ls src/clxn/*.cpp)
AI=$(shell ls src/ai/*.cpp)
CMD=$(shell ls src/cmd/*.cpp)

# list the source code
SOURCES=$(MAIN) $(CORE) $(PIECE) $(UTIL) $(EXCEPT) $(BOARD) $(CLXN) $(AI)
SOURCES += $(CMD)

# compile the source code
OBJECTS=$(SOURCES:.cpp=.o)

# define the executable name
EXECUTABLE=othello

# start of build code

# target to compile the entire project without debug symbols
all: $(SOURCES) $(EXECUTABLE)

# target to build the executable without debug symbols
$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $@ bin/
	mv -f $(OBJECTS) obj/

# target to build the executable with debug symbols
debug: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(EXECUTABLE) bin/
	mv -f $(OBJECTS) obj/

# target to compile source code to object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# target to clean the workspace
clean:
	rm -rf bin
	rm -rf obj

# target to install the compiled program
# REQUIRES ROOT
install:
	cp bin/$(EXECUTABLE) /usr/bin/

# end of Makefile
