/*
 * CmdNode.h
 * Declares a class that represents a part of an Othello command
 * Created on 11/26/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "NodeType.h"
#include <string>
#include <cctype>

//class declaration
class CmdNode final {
	//public fields and methods
	public:
		//constructor
		CmdNode(NodeType newType, const std::string& newValue);

		//destructor
		~CmdNode();

		//copy constructor
		CmdNode(const CmdNode& cn);

		//move constructor
		CmdNode(CmdNode&& cn);

		//assignment operator
		CmdNode& operator=(const CmdNode& src);

		//move operator
		CmdNode& operator=(CmdNode&& src);

		//getter methods
		
		//returns the node's type
		NodeType getType() const;

		//returns the node's value as a string
		const std::string& stringValue() const;

		//returns the node's value as an integer
		//returns -1 if the node's value is not an integer
		int intValue() const;

	//private fields and methods
	private:
		//fields
		NodeType type; //the type of the node
		std::string value; //the value of the node
};

//end of class
