/*
 * CmdReader.h
 * Declares a class that reads in Othello commands
 * Created on 11/27/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <cppcurses/cppcurses.h>
#include <vector>
#include <string>
#include "../util/Constants.h"
#include "CmdStringList.h"

//class declaration
class CmdReader final {
	//public fields and methods
	public:
		//constructor
		CmdReader();

		//destructor
		~CmdReader();

		//copy constructor
		CmdReader(const CmdReader& cr);

		//move constructor
		CmdReader(CmdReader&& cr);

		//assignment operator
		CmdReader& operator=(const CmdReader& src);

		//move operator
		CmdReader& operator=(CmdReader&& src);

		//other method
		
		//reads in a command and splits it into a
		//vector of strings
		CmdStringList prompt();

	//private fields and methods
	private:
		//fields
		int xPos; //the x-coordinate of the prompt bar
		int yPos; //the y-coordinate of the prompt bar

};

//end of file
