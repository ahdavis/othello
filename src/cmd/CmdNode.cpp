/*
 * CmdNode.cpp
 * Implements a class that represents a part of an Othello command
 * Created on 11/27/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "CmdNode.h"

//constructor
CmdNode::CmdNode(NodeType newType, const std::string& newValue)
	: type(newType), value(newValue) //init the fields
{
	//no code needed
}

//destructor
CmdNode::~CmdNode() {
	//no code needed
}

//copy constructor
CmdNode::CmdNode(const CmdNode& cn)
	: type(cn.type), value(cn.value) //copy the fields
{
	//no code needed
}

//move constructor
CmdNode::CmdNode(CmdNode&& cn)
	: type(cn.type), value(cn.value) //move the fields
{
	//no code needed
}

//assignment operator
CmdNode& CmdNode::operator=(const CmdNode& src) {
	this->type = src.type; //assign the type field
	this->value = src.value; //assign the value field
	return *this; //and return the object
}

//move operator
CmdNode& CmdNode::operator=(CmdNode&& src) {
	this->type = src.type; //move the type field
	this->value = src.value; //move the value field
	return *this; //and return the object
}

//getType method - returns the node's type
NodeType CmdNode::getType() const {
	return this->type; //return the type field
}

//stringValue method - returns the node's value as a string
const std::string& CmdNode::stringValue() const {
	return this->value; //return the value field
}

//intValue method - returns the node's value as an integer
//Returns -1 if the node does not contain an integer
int CmdNode::intValue() const {
	//verify that the value is an integer
	if(std::isdigit(this->value[0])) { //it's an integer
		return std::stoi(this->value); //return the value as an int
	} else { //not an integer
		return -1; //return an error
	}
}

//end of implementation
