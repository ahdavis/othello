/*
 * CmdParser.cpp
 * Implements a class that parses Othello commands and puts them in nodes
 * Created on 11/27/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "CmdParser.h"

//constructor
CmdParser::CmdParser(const CmdStringList& newCmds)
	: data(newCmds) //init the field
{
	//no code needed
}

//destructor
CmdParser::~CmdParser() {
	//no code needed
}

//copy constructor
CmdParser::CmdParser(const CmdParser& cp)
	: data(cp.data) //copy the field
{
	//no code needed
}

//move constructor
CmdParser::CmdParser(CmdParser&& cp)
	: data(cp.data) //move the field
{
	//no code needed
}

//assignment operator
CmdParser& CmdParser::operator=(const CmdParser& src) {
	this->data = src.data; //assign the field
	return *this; //and return the object
}

//move operator
CmdParser& CmdParser::operator=(CmdParser&& src) {
	this->data = src.data; //move the field
	return *this; //and return the object
}

//parse method - parses a string list and returns a node list
CmdNodeList CmdParser::parse() const {
	//create a return list
	CmdNodeList ret;

	//loop through the supplied strings and parse them
	for(const std::string& s : this->data) {
		//get the node type for the string
		NodeType type = this->getNodeType(s);

		//create a node from the string
		CmdNode node(type, s);

		//and add it to the list
		ret.push_back(node);
	}

	//return the node list
	return ret;
}

//private getNodeType method - returns the node type for a given string
NodeType CmdParser::getNodeType(const std::string& value) const {
	//determine the proper node type
	if(value == "save") { //save command
		return NodeType::SAV; //return the save node type
	} else if(value == "load") { //load command
		return NodeType::LOD; //return the load node type
	} else if(value == "quit") { //quit command
		return NodeType::QUT; //return the quit node type
	} else if(value == "move") { //move command
		return NodeType::MOV; //return the move node type
	} else if(std::isdigit(value[0])) { //number
		return NodeType::NUM; //return the number node type
	} else { //string
		return NodeType::STR; //return the string node type
	}
}

//end of implementation
