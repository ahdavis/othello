/*
 * CmdParser.h
 * Declares a class that parses Othello commands and puts them in nodes
 * Created on 11/27/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <cctype>
#include <string>
#include "CmdNode.h"
#include "CmdNodeList.h"
#include "CmdStringList.h"
#include "NodeType.h"

//class declaration
class CmdParser final {
	//public fields and methods
	public:
		//constructor
		explicit CmdParser(const CmdStringList& newCmds);

		//destructor
		~CmdParser();

		//copy constructor
		CmdParser(const CmdParser& cp);

		//move constructor
		CmdParser(CmdParser&& cp);

		//assignment operator
		CmdParser& operator=(const CmdParser& src);

		//move operator
		CmdParser& operator=(CmdParser&& src);

		//other methods
		
		//returns a node list containing the parsed command
		CmdNodeList parse() const;

	//private fields and methods
	private:
		//method
		
		//returns the node type for a given string
		NodeType getNodeType(const std::string& value) const;

		//field
		CmdStringList data;
};

//end of class
