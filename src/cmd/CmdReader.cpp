/*
 * CmdReader.cpp
 * Implements a class that reads in Othello commands
 * Created on 11/27/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "CmdReader.h"

//constructor
CmdReader::CmdReader()
	: xPos(CMD_OFS_X), yPos(0) //init the fields
{
	//calculate the y coordinate
	//the BOARD_Y - 1 value accounts for the "shrinking" of the board
	//to make it look better
	int boardYEnd = (BOARD_Y * SPACE_DIM) + BOARD_OFS_Y - (BOARD_Y - 1);
	this->yPos = boardYEnd + CMD_OFS_Y;
}

//destructor
CmdReader::~CmdReader() {
	//no code needed
}

//copy constructor
CmdReader::CmdReader(const CmdReader& cr)
	: xPos(cr.xPos), yPos(cr.yPos) //copy the fields
{
	//no code needed
}

//move constructor
CmdReader::CmdReader(CmdReader&& cr)
	: xPos(cr.xPos), yPos(cr.yPos) //move the fields
{
	//no code needed
}

//assignment operator
CmdReader& CmdReader::operator=(const CmdReader& src) {
	this->xPos = src.xPos; //assign the x-coord
	this->yPos = src.yPos; //assign the y-coord
	return *this; //and return the object
}

//move operator
CmdReader& CmdReader::operator=(CmdReader&& src) {
	this->xPos = src.yPos; //move the x-coord
	this->yPos = src.yPos; //move the y-coord
	return *this; //and return the object
}

//prompt method - prompts for a command and splits it into
//a vector of strings
CmdStringList CmdReader::prompt() {
	//create an output stream to print the prompt
	cppcurses::omovablestream oms;

	//set the stream's coordinates
	oms.X(this->xPos);
	oms.Y(this->yPos);

	//and print the prompt
	oms.putchar('>');
	cppcurses::reload();
	
	//create a window
	cppcurses::window* win = 
		new cppcurses::window(CMD_DIM_X, 
					CMD_DIM_Y, 
					this->xPos + 2,
					this->yPos);
	
	//create a windowed input stream to read the input
	cppcurses::iwindowstream iws(win);

	//clear the window
	cppcurses::clrwin(win);
	cppcurses::reload(win);

	//create the vector to be returned
	CmdStringList ret;

	//create a string buffer to read in the command
	std::string buf;

	//loop and read in the command
	while(iws >> buf) {
		ret.push_back(buf); //add the string buffer to the array

		//handle EOL
		if(iws.peek() == '\n') { //if the next character is EOL
			break; //then exit the loop
		}
	}

	//clear the window again
	cppcurses::clrwin(win);
	cppcurses::reload(win);

	//deallocate the window
	delete win;
	win = nullptr;

	//and return the vector
	return ret;
}

//end of implementation
