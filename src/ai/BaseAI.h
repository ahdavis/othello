/*
 * BaseAI.h
 * Declares an abstract class that represents an Othello AI
 * Created on 11/22/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <limits>
#include <algorithm>
#include "../clxn/MoveList.h"
#include "../clxn/ScoreList.h"
#include "../board/Move.h"
#include "../core/GameState.h"
#include "../player/PlayerID.h"

//class declaration
class BaseAI {
	//public fields and methods
	public:
		//constructor
		explicit BaseAI(const GameState& start);

		//destructor
		virtual ~BaseAI();

		//copy constructor
		BaseAI(const BaseAI& bai);

		//move constructor
		BaseAI(BaseAI&& bai);

		//assignment operator
		BaseAI& operator=(const BaseAI& src);

		//move operator
		BaseAI& operator=(BaseAI&& src);

		//abstract method
		
		//returns the next move chosen by the AI
		virtual const Move& getNextMove() = 0;

	//protected fields and methods
	protected:
		//methods
		
		//returns the score of a given game state
		int score(const GameState& current, int depth) const;

		//finds the best move and sets the nextMove field
		//accordingly, returning a value used to calculate the
		//move recursively
		int minimax(const GameState& current, int curDepth,
				int maxDepth, bool isAI, 
					int alpha, int beta);

		//fields
		Move nextMove; //the best move according to the AI
		GameState state; //the starting game state for the AI
		
};

//end of class
