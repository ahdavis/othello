/*
 * BaseAI.cpp
 * Implements an abstract class that represents an Othello AI
 * Created on 11/22/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "BaseAI.h"

//constructor
BaseAI::BaseAI(const GameState& start)
	: nextMove(1, 1, PlayerID::RED), state(start) //init the fields
{
	//no code needed
}

//destructor
BaseAI::~BaseAI() {
	//no code needed
}

//copy constructor
BaseAI::BaseAI(const BaseAI& bai)
	: nextMove(bai.nextMove), state(bai.state) //copy the fields
{
	//no code needed
}

//move constructor
BaseAI::BaseAI(BaseAI&& bai)
	: nextMove(bai.nextMove), state(bai.state) //move the fields
{
	//no code needed
}

//assignment operator
BaseAI& BaseAI::operator=(const BaseAI& src) {
	this->nextMove = src.nextMove; //assign the next move
	this->state = src.state; //assign the game state
	return *this; //and return the object
}

//move operator
BaseAI& BaseAI::operator=(BaseAI&& src) {
	this->nextMove = src.nextMove; //move the next move
	this->state = src.state; //move the game state
	return *this; //and return the object
}

//protected score method - returns the score of a given game state
int BaseAI::score(const GameState& current, int depth) const {
	//localize the human player
	PlayerID humanPlayer = current.getHumanPlayer();

	//localize the red and blue powers
	int redPower = current.getRedPower();
	int bluePower = current.getBluePower();

	//handle a tie
	if(redPower == bluePower) {
		return 0; //no one won, so return a 0
	}

	//handle red being the human player
	if(humanPlayer == PlayerID::RED) {
		//check if the red player won
		if(redPower > bluePower) {
			return 10 - depth; //return a depth-adjusted score
		} else { //blue player won
			return depth - 10; //return a depth-adjusted score
		}
	} else { //blue is the human player
		//check if the blue player won
		if(bluePower > redPower) {
			return 10 - depth; //return a depth-adjusted score
		} else { //red player won
			return depth - 10; //return a depth-adjusted score
		}
	}
}

//protected minimax method - finds the best move recursively
int BaseAI::minimax(const GameState& current, 
			int curDepth, int maxDepth, bool isAI,
				int alpha, int beta) {
	//handle the end of game/end of search condition
	if(current.isOver() || (curDepth >= maxDepth)) {
		return this->score(current, curDepth); //return the score
	}

	//calculate the best move
	
	//create a list to store the scores
	ScoreList scores;
	
	//handle the AI move
	if(isAI) {
		//initialize the best score
		int bestScore = std::numeric_limits<int>::min();

		//get the possible moves
		MoveList possMoves = current.getMoves();

		//loop through the possible moves
		for(int i = 0; i < possMoves.length(); i++) {
			//get a new game state using the possible moves
			GameState node(current, possMoves[i]);

			//get the best score recursively
			int score = this->minimax(node, curDepth + 1,
					maxDepth, false, alpha, beta);

			//add it to the score list
			scores.push(score);

			//maximize the score
			bestScore = std::max(bestScore, score);

			//adjust the alpha value
			alpha = std::max(alpha, bestScore);

			//prune the tree
			if(beta <= alpha) {
				break;
			}

			//assign the best move
			this->nextMove = 
				possMoves[scores.indexOf(bestScore)];

		}

		//and return the best score
		return bestScore;
	
	} else { //handle the simulated player move
		int bestScore = std::numeric_limits<int>::max();

		//get the possible moves
		MoveList possMoves = current.getMoves();

		//loop through the possible moves
		for(int i = 0; i < possMoves.length(); i++) {
			//get a new game state using the possible moves
			GameState node(current, possMoves[i]);

			//get the best score recursively
			int score = this->minimax(node, curDepth + 1,
					maxDepth, true, alpha, beta);

			//add the score to the list
			scores.push(score);

			//minimize the best score
			bestScore = std::min(bestScore, score);

			//adjust the beta
			beta = std::min(beta, bestScore);

			//prune the tree
			if(beta <= alpha) {
				break;
			}

			//assign the best move
			this->nextMove = 
				possMoves[scores.indexOf(bestScore)];

		}

		//and return the best score
		return bestScore;
	}
}

//end of implementation
