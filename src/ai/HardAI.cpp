/*
 * HardAI.cpp
 * Implements a class that represents an Othello AI on hard mode
 * Created on 11/22/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "HardAI.h"

//constructor
HardAI::HardAI(const GameState& start)
	: BaseAI(start) //call the superclass constructor
{
	//no code needed
}

//destructor
HardAI::~HardAI() {
	//no code needed
}

//copy constructor
HardAI::HardAI(const HardAI& hai)
	: BaseAI(hai) //call the superclass copy constructor
{
	//no code needed
}

//move constructor
HardAI::HardAI(HardAI&& hai)
	: BaseAI(hai) //call the superclass move constructor
{
	//no code needed
}

//assignment operator
HardAI& HardAI::operator=(const HardAI& src) {
	BaseAI::operator=(src); //call the superclass assignment operator
	return *this; //and return the object
}

//move operator
HardAI& HardAI::operator=(HardAI&& src) {
	BaseAI::operator=(src); //call the superclass move operator
	return *this; //and return the object
}

//overridden getNextMove method - returns the next move derived by the AI
const Move& HardAI::getNextMove() {
	//get whether the AI should play
	bool isAI = this->state.getCurrentPlayer() 
			!= this->state.getHumanPlayer();

	//calculate the best move
	this->minimax(this->state, 0, HARD_DEPTH, isAI, 
			std::numeric_limits<int>::min(), 
				std::numeric_limits<int>::max());
	
	return this->nextMove; //and return it

}

//end of implementation
