/*
 * HardAI.h
 * Declares a class that represents an Othello AI on hard mode
 * Created on 11/22/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <limits>
#include "BaseAI.h"
#include "../util/Constants.h"
#include "../core/GameState.h"
#include "../board/Move.h"

//class declaration
class HardAI final : public BaseAI
{
	//public fields and methods
	public:
		//constructor
		explicit HardAI(const GameState& start);

		//destructor
		~HardAI();

		//copy constructor
		HardAI(const HardAI& hai);

		//move constructor
		HardAI(HardAI&& hai);

		//assignment operator
		HardAI& operator=(const HardAI& src);

		//move operator
		HardAI& operator=(HardAI&& src);

		//overridden getNextMove method - returns the next move
		//derived by the AI
		const Move& getNextMove() override;

	//no protected/private members
};

//end of class
