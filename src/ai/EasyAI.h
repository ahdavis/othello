/*
 * EasyAI.h
 * Declares a class that represents an Othello AI on easy mode
 * Created on 11/22/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <limits>
#include "BaseAI.h"
#include "../util/Constants.h"
#include "../core/GameState.h"
#include "../board/Move.h"

//class declaration
class EasyAI final : public BaseAI
{
	//public fields and methods
	public:
		//constructor
		explicit EasyAI(const GameState& start);

		//destructor
		~EasyAI();

		//copy constructor
		EasyAI(const EasyAI& eai);

		//move constructor
		EasyAI(EasyAI&& eai);

		//assignment operator
		EasyAI& operator=(const EasyAI& src);

		//move operator
		EasyAI& operator=(EasyAI&& src);

		//overridden getNextMove method - returns the next move
		//derived by the AI
		const Move& getNextMove() override;

	//no protected/private members
};

//end of class
