/*
 * InvalidCoordsException.h
 * Declares an exception that is thrown when invalid coordinates
 * are encountered
 * Created on 11/12/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <exception>
#include "../util/CoordPair.h"

//class declaration
class InvalidCoordsException final : public std::exception
{
	//public fields and methods
	public:
		//first constructor - constructs from x and y coordinates
		InvalidCoordsException(int x, int y);

		//second constructor - constructs from a CoordPair object
		explicit InvalidCoordsException(const CoordPair& cp);

		//destructor
		~InvalidCoordsException();

		//copy/move constructors and assignment/move operators
		//are defaulted

		//other methods
		
		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the actual error message

};

//end of class
