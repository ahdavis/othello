/*
 * IndexException.h
 * Declares an exception that is thrown on an invalid access index
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//class declaration
class IndexException final : public std::exception
{
	//public fields and methods
	public:
		//constructor
		explicit IndexException(int badIndex);

		//destructor
		~IndexException();

		//copy constructor is defaulted
		
		//move constructor is defaulted
		
		//assignment operator is defaulted
		
		//move operator is defaulted
		
		//other methods
		
		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the actual error message
};

//end of class
