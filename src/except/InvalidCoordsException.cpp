/*
 * InvalidCoordsException.cpp
 * Implements an exception that is thrown when invalid coordinates
 * are encountered
 * Created on 11/12/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "InvalidCoordsException.h"

//first constructor
InvalidCoordsException::InvalidCoordsException(int x, int y)
	: errMsg() //init the error message
{
	//assemble the error message
	this->errMsg = "Invalid coordinates: (" + std::to_string(x)
			+ "," + std::to_string(y) + ")";
}

//second constructor
InvalidCoordsException::InvalidCoordsException(const CoordPair& cp)
	: InvalidCoordsException(cp.getX(), cp.getY()) //call other ctor
{
	//no code needed
}

//destructor
InvalidCoordsException::~InvalidCoordsException() {
	//no code needed
}

//overridden what method - called when the exception is thrown
const char* InvalidCoordsException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
