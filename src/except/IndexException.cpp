/*
 * IndexException.cpp
 * Implements an exception that is thrown on an invalid access index
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "IndexException.h"

//constructor
IndexException::IndexException(int badIndex)
	: errMsg() //init the error message field
{
	//assemble the error message
	this->errMsg = "Invalid index: " + std::to_string(badIndex);
}

//destructor
IndexException::~IndexException() {
	//no code needed
}

//overridden what method - called when the exception is thrown
const char* IndexException::what() const throw() {
	return this->errMsg.c_str(); //return the error as a C string
}

//end of implementation
