/*
 * RedPiece.cpp
 * Implements a class that represents a red Othello piece
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "RedPiece.h"

//constructor 1
RedPiece::RedPiece(const CoordPair& newCoords)
	: Piece(EnumPiece::RED, newCoords) //call the superclass constructor
{
	//no code needed
}

//constructor 2
RedPiece::RedPiece(int newX, int newY)
	: Piece(EnumPiece::RED, newX, newY) //call superclass constructor
{
	//no code needed
}

//destructor
RedPiece::~RedPiece() {
	//no code needed
}

//copy constructor
RedPiece::RedPiece(const RedPiece& rp)
	: Piece(rp) //call the superclass copy constructor
{
	//no code needed
}

//move constructor
RedPiece::RedPiece(RedPiece&& rp)
	: Piece(rp) //call the superclass move constructor
{
	//no code needed
}

//assignment operator
RedPiece& RedPiece::operator=(const RedPiece& src) {
	Piece::operator=(src); //call the superclass assignment operator
	return *this; //and return the object
}

//move operator
RedPiece& RedPiece::operator=(RedPiece&& src) {
	Piece::operator=(src); //call the superclass move operator
	return *this; //and return the object
}

//serialization code

//serialization operator
std::ostream& operator<<(std::ostream& os, const RedPiece& rp) {
	//cast the RedPiece to a Piece and serialize that
	os << static_cast<const Piece&>(rp);

	//and return the stream
	return os;
}

//deserialization operator
std::istream& operator>>(std::istream& is, RedPiece& rp) {
	//cast the RedPiece to a Piece and deserialize that
	is >> static_cast<Piece&>(rp);

	//and return the stream
	return is;
}

//end of implementation
