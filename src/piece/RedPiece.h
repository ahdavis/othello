/*
 * RedPiece.h
 * Declares a class that represents a red Othello piece
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include "EnumPiece.h"
#include "Piece.h"
#include "../util/CoordPair.h"

//class declaration
class RedPiece final : public Piece
{
	//public fields and methods
	public:
		//constructor 1
		explicit RedPiece(const CoordPair& newCoords);

		//constructor 2
		RedPiece(int newX, int newY);

		//destructor
		~RedPiece();

		//copy constructor
		RedPiece(const RedPiece& rp);

		//move constructor
		RedPiece(RedPiece&& rp);

		//assignment operator
		RedPiece& operator=(const RedPiece& src);

		//move operator
		RedPiece& operator=(RedPiece&& src);

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const RedPiece& rp);
		
		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						RedPiece& rp);
};

//end of class
