/*
 * Piece.cpp
 * Implements a class that represents an Othello piece
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Piece.h"

//constructor 1
Piece::Piece(EnumPiece newType, int newX, int newY)
	: pieceType(newType), coords(newX, newY) //init the fields
{
	//no code needed
}

//constructor 2
Piece::Piece(EnumPiece newType, const CoordPair& newCoords)
	: Piece(newType, newCoords.getX(), newCoords.getY())
{
	//no code needed
}
			

//destructor
Piece::~Piece() {
	//no code needed
}

//copy constructor
Piece::Piece(const Piece& p)
	: pieceType(p.pieceType), coords(p.coords) //copy the fields
{ 
	//no code needed
}

//move constructor
Piece::Piece(Piece&& p)
	: pieceType(p.pieceType), coords(p.coords) //move the fields
{
	//no code needed
}

//assignment operator
Piece& Piece::operator=(const Piece& src) {
	this->pieceType = src.pieceType; //assign the piece type
	this->coords = src.coords; //assign the coordinates
	return *this; //and return the object
}

//move operator
Piece& Piece::operator=(Piece&& src) {
	this->pieceType = src.pieceType; //move the piece type
	this->coords = src.coords; //move the coordinates
	return *this; //and return the object
}

//getter method

//getType method - returns the Piece's type
EnumPiece Piece::getType() const {
	return this->pieceType; //return the piece type field
}

//draw method - draws the Piece
void Piece::draw() {
	//create a movable stream to draw the Piece with
	cppcurses::omovablestream oms;

	//change the draw position to the Piece's location
	oms.X(this->coords.getX());
	oms.Y(this->coords.getY());

	//and draw the Piece
	if(this->pieceType == EnumPiece::RED) { //if the piece is red
		//then get a color pair for the piece
		cppcurses::color_pair cp = cppcurses::color_pair(
						cppcurses::color_red(),
						cppcurses::color_black());
		
		//enable it
		cp.enable();

		//draw the Piece
		oms.putchar(cppcurses::acs::diamond());
		cppcurses::reload();

		//and disable the color pair
		cp.disable();

	} else { //if the piece is blue
		//then get a color pair for the piece
		cppcurses::color_pair cp = cppcurses::color_pair(
						cppcurses::color_blue(),
						cppcurses::color_black());
		
		//enable it
		cp.enable();

		//draw the Piece
		oms.putchar(cppcurses::acs::diamond());
		cppcurses::reload();

		//and disable the color pair
		cp.disable();
	}

}


//serialization code

//serialization operator
std::ostream& operator<<(std::ostream& os, const Piece& p) {
	//convert the piece type to a integer
	int temp = static_cast<int>(p.pieceType);

	//write it to the stream
	os << temp << '\n';

	//write the coordinates to the stream
	os << p.coords << '\n';

	//and return the stream
	return os;
}

//deserialization operator
std::istream& operator>>(std::istream& is, Piece& p) {
	//read in the piece type as an integer
	int temp = 0; //used to read in the piece type
	is >> temp; 

	//assign the read-in type to the Piece
	p.pieceType = static_cast<EnumPiece>(temp);

	//read in the coordinates
	is >> p.coords;

	//and return the stream
	return is;
}

//end of implementation
