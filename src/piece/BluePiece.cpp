/*
 * BluePiece.cpp
 * Implements a class that represents a blue Othello piece
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "BluePiece.h"

//constructor 1
BluePiece::BluePiece(const CoordPair& newCoords)
	: Piece(EnumPiece::BLUE, newCoords) //call superclass constructor
{
	//no code needed
}

//constructor 2
BluePiece::BluePiece(int newX, int newY)
	: Piece(EnumPiece::BLUE, newX, newY) //call superclass constructor
{
	//no code needed
}

//destructor
BluePiece::~BluePiece() {
	//no code needed
}

//copy constructor
BluePiece::BluePiece(const BluePiece& bp)
	: Piece(bp) //call the superclass copy constructor
{
	//no code needed
}

//move constructor
BluePiece::BluePiece(BluePiece&& bp)
	: Piece(bp) //call the superclass move constructor
{
	//no code needed
}

//assignment operator
BluePiece& BluePiece::operator=(const BluePiece& src) {
	Piece::operator=(src); //call the superclass assignment operator
	return *this; //and return the object
}

//move operator
BluePiece& BluePiece::operator=(BluePiece&& src) {
	Piece::operator=(src); //call the superclass move operator
	return *this; //and return the object
}

//serialization code

//serialization operator
std::ostream& operator<<(std::ostream& os, const BluePiece& bp) {
	//cast the BluePiece to a Piece and serialize that
	os << static_cast<const Piece&>(bp);

	//and return the stream
	return os;
}

//deserialization operator
std::istream& operator>>(std::istream& is, BluePiece& bp) {
	//cast the BluePiece to a Piece and deserialize that
	is >> static_cast<Piece&>(bp);

	//and return the stream
	return is;
}

//end of implementation
