/*
 * Piece.h
 * Declares a class that represents an Othello piece
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "EnumPiece.h"
#include <iostream>
#include <cppcurses/cppcurses.h>
#include "../util/CoordPair.h"

//class declaration
class Piece {
	//public fields and methods
	public:
		//constructor 1
		Piece(EnumPiece newType, int newX, int newY);

		//constructor 2
		Piece(EnumPiece newType, const CoordPair& newCoords);

		//destructor
		virtual ~Piece();

		//copy constructor
		Piece(const Piece& p);

		//move constructor
		Piece(Piece&& p);

		//assignment operator
		Piece& operator=(const Piece& src);

		//move operator
		Piece& operator=(Piece&& src);

		//getter method
		
		//returns the Piece's type
		EnumPiece getType() const;

		//draws the Piece
		void draw();

		//serialization code
		
		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const Piece& p);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						Piece& p);

	//protected fields and methods
	protected:
		//field
		EnumPiece pieceType; //the type of the Piece
		CoordPair coords; //the coordinates of the Piece
};

//end of class
