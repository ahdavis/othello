/*
 * main.cpp
 * Main code file for Othello
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <cstdlib>
#include "core/Othello.h"
#include <cppcurses/cppcurses.h>

//main function - main entry point for program
int main(int argc, char* argv[]) {
	//start up cppcurses
	cppcurses::start();

	//remove the cursor
	cppcurses::set_cursor_mode(cppcurses::enum_cursor_mode::invisible);

	//use the Othello class to run the game
	Othello::getInstance().run();

	//replace the cursor
	cppcurses::set_cursor_mode(cppcurses::enum_cursor_mode::normal);

	//close down cppcurses
	cppcurses::end();

	//and return with no errors
	return EXIT_SUCCESS;

}

//end of program
