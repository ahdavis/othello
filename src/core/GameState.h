/*
 * GameState.h
 * Declares a class that represents a game state for Othello
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <cmath>
#include <cppcurses/cppcurses.h>
#include "../board/Board.h"
#include "../board/Move.h"
#include "../board/RedMove.h"
#include "../board/BlueMove.h"
#include "../board/SpaceState.h"
#include "../clxn/MoveList.h"
#include "../clxn/ScoreList.h"
#include "../util/CoordPair.h"
#include "../board/Orient.h"
#include "../player/PlayerID.h"
#include "../util/Constants.h"

//class declaration
class GameState final {
	//public fields and methods
	public:
		//constructor 1 - constructs from an orientation and an ID
		GameState(Orient newOrient, PlayerID starter, 
						PlayerID human);

		//constructor 2 - used by the AI to get the next state
		GameState(const GameState& src, const Move& nextMove);

		//destructor
		~GameState();

		//copy constructor
		GameState(const GameState& gs);

		//move constructor
		GameState(GameState&& gs);

		//assignment operator
		GameState& operator=(const GameState& src);

		//move operator
		GameState& operator=(GameState&& src);

		//getter methods
		
		//returns the state of a board space at given coords
		SpaceState getStateAtCoords(int x, int y) const;

		//returns the state of a board space at a given CoordPair
		SpaceState getStateAtCoords(const CoordPair& coords) const;

		//returns a list of the legal moves for the current state
		MoveList getMoves() const;

		//returns whether the game is over
		bool isOver() const;

		//returns the number of red pieces on the board
		int getRedPower() const;

		//returns the number of blue pieces on the board
		int getBluePower() const;

		//returns the number of empty spaces on the board
		int getEmptyPower() const;

		//returns the ID of the current player
		PlayerID getCurrentPlayer() const;

		//returns the ID of the human player
		PlayerID getHumanPlayer() const;

		//other methods
		
		//returns whether a given move is legal
		bool isMoveLegal(const Move& m) const;

		//returns a list of adjacent enemies for a given move
		MoveList getAdjacentEnemies(const Move& m) const;

		//executes a move
		void doMove(const Move& m);

		//updates the state
		void update();

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const GameState& gs);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						GameState& gs);

	//private fields and methods
	private:
		//methods
		
		//executes a move and updates the turn status
		void doMove(const Move& m, bool flip);
		
		//returns the possible moves for a given board space
		MoveList getPossMoves(const CoordPair& center) const;

		//returns the total number of spaces with a specific state
		int getGeneralPower(SpaceState testState) const;

		//fields
		Board board; //the board used to play on
		PlayerID curPlayer; //the current player
		PlayerID humanPlayer; //the human player
		
};

//end of class
