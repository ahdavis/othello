/*
 * GameEngine.h
 * Declares a class that runs the primary code for Othello
 * Created on 11/28/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <fstream>
#include <string>
#include <cmath>
#include <cppcurses/cppcurses.h>
#include "../ai/AIType.h"
#include "../ai/BaseAI.h"
#include "../ai/EasyAI.h"
#include "../ai/HardAI.h"
#include "../ai/MedAI.h"
#include "GameState.h"
#include "../board/Move.h"
#include "../board/RedMove.h"
#include "../board/BlueMove.h"
#include "../util/CoordPair.h"
#include "../cmd/CmdNodeList.h"
#include "../cmd/CmdStringList.h"
#include "../cmd/CmdNode.h"
#include "../cmd/CmdParser.h"
#include "../cmd/NodeType.h"
#include "../cmd/CmdReader.h"
#include "../player/PlayerID.h"
#include "../board/Orient.h"
#include "../board/SpaceState.h"
#include "../util/fileExists.h"
#include "../util/Constants.h"
#include "../clxn/MoveList.h"

//class declaration
class GameEngine final {
	//public fields and methods
	public:
		//constructor
		GameEngine(AIType newDiff, bool playerFirst, 
				Orient newOrient);

		//destructor
		~GameEngine();

		//copy constructor
		GameEngine(const GameEngine& ge);

		//move constructor
		GameEngine(GameEngine&& ge);

		//assignment operator
		GameEngine& operator=(const GameEngine& src);

		//move operator
		GameEngine& operator=(GameEngine&& src);

		//other method
		
		//runs the game
		void play();

	//private fields and methods
	private:
		//methods
		
		//deallocates the memory used by the engine
		void free();

		//saves the game
		void save(const std::string& saveFile);

		//loads the game from a save
		void load(const std::string& loadFile);

		//prompts for a command and returns its node list
		CmdNodeList getCommand();

		//updates the info box
		void updateInfo();

		//dispatches a command
		//returns false if there is an error with the command
		bool dispatchCmd(const CmdNodeList& nodes);

		//handles the end of the game
		void handleEnd();

		//gets the next move from the AI
		Move getAIMove();

		//executes a move
		void doMove(const Move& m);

		//fields
		
		GameState* state; //holds the current state of the game
		AIType diff; //the difficulty of the game
		bool running; //is the game running?

};

//end of class
