/*
 * Othello.cpp
 * Implements a singleton class that runs Othello
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Othello.h"

//private constructor
Othello::Othello()
	: engine(nullptr) //null the field
{
	this->setup(); //set up the game
}

//destructor
Othello::~Othello() {
	//deallocate the engine
	delete this->engine;
	this->engine = nullptr;
}

//getter methods

//static getInstance method - returns an instance of the class
Othello& Othello::getInstance() {
	//create an autodestruct instance of the class
	static Othello instance;

	//and return it
	return instance;
}

//other methods

//run method - runs the game
void Othello::run() {
	this->engine->play(); //use the game engine to play the game
}

//private setup method - sets up the game
void Othello::setup() {
	//clear the screen
	cppcurses::clrscr();
	cppcurses::reload();

	//create a movable output stream
	cppcurses::omovablestream oms;

	//create a movable input stream
	cppcurses::imovablestream ims;

	//define the prompt strings
	std::string firstPrompt = "Do you want to go first? (y/n) ";
	std::string orientPrompt = "Sideways or vertical board? (s/v) ";
	std::string diffPrompt = "Easy, Medium, or Hard? (e/m/h) ";

	//declare the variables that will be read in
	bool first;
	Orient orient;
	AIType aiType;

	//get the locations to display the prompts
	int firstX = floor((80 - firstPrompt.length()) / 2);
	int firstY = 11;
	int orientX = floor((80 - orientPrompt.length()) / 2);
	int orientY = 12;
	int diffX = floor((80 - diffPrompt.length()) / 2);
	int diffY = 13;

	//move the output stream to the first location
	oms.X(firstX);
	oms.Y(firstY);

	//display the first prompt
	oms << firstPrompt;

	//read in the answer
	char r1 = 'q'; //holds the answer

	//loop and read in the answer
	while((std::tolower(r1) != 'y') && (std::tolower(r1) != 'n')) {
		//set the input stream's coordinates
		ims.X(oms.X() + 1);
		ims.Y(oms.Y());

		//read in the answer
		ims >> r1;
	}

	//init the first play flag
	first = std::tolower(r1) == 'y' ? true : false;

	//move the output stream to the second location
	oms.X(orientX);
	oms.Y(orientY);

	//display the second prompt
	oms << orientPrompt;

	//read in the answer
	char r2 = 'q'; //holds the answer

	//loop and read in the answer
	while((std::tolower(r2) != 's') && (std::tolower(r2) != 'v')) {
		//set the input stream's coordinates
		ims.X(oms.X() + 1);
		ims.Y(oms.Y());

		//read in the answer
		ims >> r2;
	}

	//init the orientation value
	if(first) { //if the player goes first
		if(std::tolower(r2) == 's') { //if the board is sideways
			orient = Orient::RED_L; //then init the flag
		} else { //vertical board
			orient = Orient::RED_F; //init the flag
		}
	} else { //player goes second
		if(std::tolower(r2) == 's') { //if the board is sideways
			orient = Orient::BLUE_L; //then init the flag
		} else { //vertical board
			orient = Orient::BLUE_F; //init the flag
		}
	}

	//move the output stream to the third location
	oms.X(diffX);
	oms.Y(diffY);

	//display the second prompt
	oms << diffPrompt;

	//read in the answer
	char r3 = 'q'; //holds the answer

	//loop and read in the answer
	while((std::tolower(r3) != 'e') && (std::tolower(r3) != 'm')
			&& (std::tolower(r3) != 'h')) {
		//set the input stream's coordinates
		ims.X(oms.X() + 1);
		ims.Y(oms.Y());

		//read in the answer
		ims >> r3;
	}

	//init the difficulty value
	if(std::tolower(r3) == 'e') { //easy difficulty
		aiType = AIType::EASY; //make the difficulty easy
	} else if(std::tolower(r3) == 'm') { //medium difficulty
		aiType = AIType::MED; //make the difficulty medium
	} else { //hard difficulty
		aiType = AIType::HARD; //make the difficulty hard
	}

	//initialize the game engine field
	this->engine = new GameEngine(aiType, first, orient);

}

//end of implementation
