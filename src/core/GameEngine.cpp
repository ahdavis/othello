/*
 * GameEngine.cpp
 * Implements a class that runs the primary code for Othello
 * Created on 11/28/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "GameEngine.h"

//constructor
GameEngine::GameEngine(AIType newDiff, bool playerFirst, Orient newOrient)
	: state(nullptr), diff(newDiff), running(true) //init the fields
{
	//initialize the game state pointer
	
	//get the ID of the human player
	PlayerID human = playerFirst ? PlayerID::RED : PlayerID::BLUE;

	//get the ID of the starting player
	PlayerID starter = PlayerID::RED;

	//allocate the game state
	this->state = new GameState(newOrient, starter, human);
}

//destructor
GameEngine::~GameEngine() {
	this->free(); //deallocate the engine
}

//copy constructor
GameEngine::GameEngine(const GameEngine& ge)
	: state(nullptr), diff(ge.diff), running(ge.running)
{
	//copy the state field
	this->state = new GameState(*ge.state);
}

//move constructor
GameEngine::GameEngine(GameEngine&& ge)
	: state(nullptr), diff(ge.diff), running(ge.running) 
{
	//move the state field
	this->state = new GameState(*ge.state);

	//and free the temporary object
	ge.free();
}

//assignment operator
GameEngine& GameEngine::operator=(const GameEngine& src) {
	//deallocate the current state field
	this->free();

	//assign the state field
	this->state = new GameState(*src.state);

	//assign the difficulty field
	this->diff = src.diff;

	//assign the running flag
	this->running = src.running;

	//and return the object
	return *this;
}

//move operator
GameEngine& GameEngine::operator=(GameEngine&& src) {
	//deallocate the current state field
	this->free();

	//move the state field
	this->state = new GameState(*src.state);

	//move the difficulty field
	this->diff = src.diff; 

	//assign the running flag
	this->running = src.running;

	//deallocate the temporary
	src.free();

	//and return the object
	return *this;
}

//play method - runs the game
void GameEngine::play() {

	//loop and run the game
	while(true) { //loop forever

		//make sure the game is still running
		if(!this->running) {
			break;
		}
		
		cppcurses::clrscr();

		//update the board
		this->state->update();

		//update the info box
		this->updateInfo();

		//prompt for a command
		CmdNodeList nodes = this->getCommand();

		//process the command
		bool success = this->dispatchCmd(nodes);

		//make sure the command succeeded
		if(!success) { //if the command failed
			continue; //then restart the turn
		}

		//check to make sure the game is still running
		if(!this->running) {
			break;
		}

		//get the AI's move
		Move aiMove = this->getAIMove();

		//and execute it
		this->doMove(aiMove);

		//update the board again
		this->state->update();

		//handle whether the game is over
		if(this->state->isOver()) {
			cppcurses::wait_key();
			break;
		}
	}

	//handle the end
	this->handleEnd();
}

//private free method - deallocates the memory used by the engine
void GameEngine::free() {
	delete this->state; //deallocate the state
	this->state = nullptr; //and zero it out
}

//private save method - saves the game
void GameEngine::save(const std::string& saveFile) {
	//create a file stream to save the game
	std::ofstream saver;
	saver.open(saveFile.c_str(), std::ios::out | std::ios::trunc);

	//write the current game state to the file
	saver << *this->state;

	//and close the file
	saver.close();
}

//private load method - loads the game from a save
void GameEngine::load(const std::string& loadFile) {
	//make sure the file exists
	if(!fileExists(loadFile)) { //if the file does not exist
		return; //then exit the method
	}

	//create a file stream to load the game
	std::ifstream loader;
	loader.open(loadFile.c_str(), std::ios::in);

	//read the state from the file
	loader >> *this->state;

	//and close the file
	loader.close();
}

//private getCommand method - prompts for a command and returns its nodes
CmdNodeList GameEngine::getCommand() {
	//read the commands
	CmdReader reader; //get a command reader object
	CmdStringList strings = reader.prompt(); //use it to get commands
	
	//parse the commands
	CmdParser parser(strings); //get a command parser object
	return parser.parse(); //and parse the strings
}

//private updateInfo method - updates the info box
void GameEngine::updateInfo() {
	//create an output stream
	cppcurses::omovablestream oms;

	//create color pairs for the outputs
	cppcurses::color_pair redPair = cppcurses::color_pair(
						cppcurses::color_red(),
						cppcurses::color_black());

	cppcurses::color_pair bluePair = cppcurses::color_pair(
						cppcurses::color_blue(),
						cppcurses::color_black());

	//calculate the placement of the info box
	int placeX = BOARD_OFS_X + (BOARD_X * SPACE_DIM) + INFO_OFS_X;
	int placeY = INFO_OFS_Y;

	//calculate the spacing of the text
	int redLine = placeY;
	int blueLine = placeY + INFO_DIM_Y;

	//write the first line
	oms.X(placeX);
	oms.Y(redLine);
	redPair.enable();
	oms << "Red: " << std::to_string(this->state->getRedPower());
	redPair.disable();

	//and write the second line
	oms.X(placeX);
	oms.Y(blueLine);
	bluePair.enable();
	oms << "Blue: " << std::to_string(this->state->getBluePower());
	bluePair.disable();

	//finally, refresh the screen
	cppcurses::reload();
}

//dispatchCmd method - dispatches a command
//returns false if the command fails
bool GameEngine::dispatchCmd(const CmdNodeList& nodes) {
	//make sure that the node list has at least one node in it
	if(nodes.size() < 1) {
		return false;
	}

	//get the first node
	CmdNode first = nodes[0];

	//make sure that it is not a string or number (invalid syntax)
	if(first.getType() == NodeType::STR) {
		return false;
	} else if(first.getType() == NodeType::NUM) {
		return false;
	}

	//handle the remaining tokens
	if(first.getType() == NodeType::LOD) { //load command
		//make sure that there are two nodes
		if(nodes.size() != 2) {
			return false;
		}
		CmdNode second = nodes[1]; //get the other node

		//verify the type of the second node
		if(second.getType() != NodeType::STR) {
			return false;
		}
		
		//and execute the command
		this->load(second.stringValue());
	} else if(first.getType() == NodeType::MOV) {
		//make sure that there are three nodes
		if(nodes.size() != 3) {
			return false;
		}

		//get the current player
		PlayerID curPlayer = this->state->getCurrentPlayer();

		//get the second and third nodes
		CmdNode second = nodes[1]; 
		CmdNode third = nodes[2];

		//verify their types
		if((second.getType() != NodeType::NUM) ||
				(third.getType() != NodeType::NUM)) {
			return false;
		}

		//create a Move object to execute the move with
		Move move(second.intValue(), third.intValue(), curPlayer);
		
		//make sure the move is legal
		if(!this->state->isMoveLegal(move)) {
			//illegal move, so return
			return false;
		}

		//and execute the move
		this->doMove(move);

	} else if(first.getType() == NodeType::QUT) { //quit command
		this->running = false; //reset the running flag
	} else if(first.getType() == NodeType::SAV) { //save command
		//verify the node count
		if(nodes.size() != 2) {
			return false;
		}

		//get the second node
		CmdNode second = nodes[1];

		//verify its type
		if(second.getType() != NodeType::STR) {
			return false;
		}

		//and execute the command
		this->save(second.stringValue());
	}

	return true; //no errors found
}

//private handleEnd method - handles the end of the game
void GameEngine::handleEnd() {
	//create the output stream
	cppcurses::omovablestream oms;

	//clear the screen
	cppcurses::clrscr();
	cppcurses::reload();
			
	//create strings to denote who won
	std::string blueWin = "Blue won!";
	std::string redWin = "Red won!";
	std::string draw = "It's a draw!";

	//get the number of active red and blue pieces
	int redCount = this->state->getRedPower();
	int blueCount = this->state->getBluePower();

	//define the y-coord to display the message at
	int y = 12;

	//draw the results
	if(redCount > blueCount) { //red wins
		//create a color pair
		cppcurses::color_pair cp =
			cppcurses::color_pair(
				cppcurses::color_red(),
				cppcurses::color_black());

		//and enable it
		cp.enable();

		//get the coordinates to draw the string
		int x = floor(80 - redWin.length()) / 2;

		//set the coordinates of the stream
		oms.X(x);
		oms.Y(y);

		//and print the message
		oms << redWin;
		cppcurses::reload();

		//disable the color pair
		cp.disable();

	} else if(blueCount > redCount) { //blue wins
		//create a color pair
		cppcurses::color_pair cp =
			cppcurses::color_pair(
				cppcurses::color_blue(),
				cppcurses::color_black());

		//and enable it
		cp.enable();

		//get the coordinates to draw the string
		int x = floor(80 - blueWin.length()) / 2;

		//set the coordinates of the stream
		oms.X(x);
		oms.Y(y);

		//and print the message
		oms << blueWin;
		cppcurses::reload();

		//disable the color pair
		cp.disable();
	} else { //draw
		//create a color pair
		cppcurses::color_pair cp =
			cppcurses::color_pair(
				cppcurses::color_green(),
				cppcurses::color_black());

		//and enable it
		cp.enable();

		//get the coordinates to draw the string
		int x = floor(80 - draw.length()) / 2;

		//set the coordinates of the stream
		oms.X(x);
		oms.Y(y);

		//and print the message
		oms << draw;
		cppcurses::reload();

		//disable the color pair
		cp.disable();
	}
	
	//wait for a keypress
	cppcurses::wait_key();

	//clear the screen
	cppcurses::clrscr();
	cppcurses::reload();

	//and turn off the running flag
	this->running = false;

}

//private getAIMove method - returns the next move from the AI
Move GameEngine::getAIMove() {
	//declare the AI to get the move from
	BaseAI* ai = nullptr;

	//instantiate it
	if(this->diff == AIType::EASY) { //easy difficulty
		ai = new EasyAI(*this->state); //create an easy AI
	} else if(this->diff == AIType::MED) { //medium difficulty
		ai = new MedAI(*this->state); //create a medium AI
	} else { //hard difficulty
		ai = new HardAI(*this->state); //create a hard AI
	}

	//get the move from the AI
	Move ret = ai->getNextMove();

	//deallocate the AI
	delete ai;
	ai = nullptr; 

	//and return the move
	return ret;
}

//private doMove method - executes a move
void GameEngine::doMove(const Move& m) {
	//use the game state to execute the move
	this->state->doMove(m);

	//update the screen
	this->state->update();
}

//end of implementation
