/*
 * Othello.h
 * Declares a singleton class that runs Othello
 * Created on 11/11/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once 

//include
#include <iostream>
#include <cppcurses/cppcurses.h>
#include <cmath>
#include <string>
#include <cctype>
#include "GameEngine.h"
#include "../board/Orient.h"
#include "../ai/AIType.h"

//class declaration
class Othello final {
	//public fields and methods
	public:
		//constructor is private
		
		//destructor
		~Othello();

		//delete the copy and move constructors
		Othello(const Othello& c) = delete;
		Othello(Othello&& c) = delete;

		//delete the assignment and move operators
		Othello& operator=(const Othello& src) = delete;
		Othello& operator=(Othello&& src) = delete;

		//getter methods
		
		//returns an instance of the class
		static Othello& getInstance();

		//other methods
		
		//runs the AI
		void run();

	//private fields and methods
	private:
		//constructor
		Othello();

		//methods
		
		//sets up the game
		void setup();

		//field
		GameEngine* engine; //the game engine itself
		
};

//end of class
