/*
 * GameState.cpp
 * Implements a class that represents a game state for Othello
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "GameState.h"

//first constructor
GameState::GameState(Orient newOrient, PlayerID starter, PlayerID human)
	: board(newOrient), curPlayer(starter), humanPlayer(human)
{
	//no code needed
}

//second constructor
GameState::GameState(const GameState& src, const Move& nextMove)
	: GameState(src) //call the copy constructor
{
	//execute the next move
	this->doMove(nextMove);
}

//destructor
GameState::~GameState() {
	//no code needed
}

//copy constructor
GameState::GameState(const GameState& gs)
	: board(gs.board), curPlayer(gs.curPlayer), 
		humanPlayer(gs.humanPlayer) //copy the fields
{
	//no code needed
}

//move constructor
GameState::GameState(GameState&& gs)
	: board(gs.board), curPlayer(gs.curPlayer), 
		humanPlayer(gs.humanPlayer) //move the fields
{
	//no code needed
}

//assignment operator
GameState& GameState::operator=(const GameState& src) {
	this->board = src.board; //assign the board
	this->curPlayer = src.curPlayer; //assign the current player
	this->humanPlayer = src.humanPlayer; //assign the human player
	return *this; //and return the object
}

//move operator
GameState& GameState::operator=(GameState&& src) {
	this->board = src.board; //move the board
	this->curPlayer = src.curPlayer; //move the current player
	this->humanPlayer = src.humanPlayer; //move the human player
	return *this; //and return the object
}

//first getStateAtCoords method - returns the state of a board space at
//given coords
SpaceState GameState::getStateAtCoords(int x, int y) const {
	return this->board.getStateAtCoords(x, y); //return the state
}

//second getStateAtCoords method - returns the state of a board space
//at a given CoordPair
SpaceState GameState::getStateAtCoords(const CoordPair& coords) const {
	//call the other method
	return this->getStateAtCoords(coords.getX(), coords.getY());
}

//getMoves method - returns a list of the possible moves 
//for the current state
MoveList GameState::getMoves() const {
	//declare the return value
	MoveList ret;

	//loop over the board
	for(int x = 1; x <= BOARD_X; x++) {
		for(int y = 1; y <= BOARD_Y; y++) {
			//translate the current player into a space state
			SpaceState test = 
				(this->curPlayer == PlayerID::RED) ?
				SpaceState::RED : SpaceState::BLUE;

			//get the state at the given coordinates
			SpaceState cur = this->getStateAtCoords(x, y);
			
			//compare the states
			if(test == cur) {
				//get the possible moves
				CoordPair temp(x, y);
				MoveList poss = this->getPossMoves(temp);

				//and copy the possible moves over to the
				//return list
				while(poss.length() > 0) {
					ret.push(poss.pop());
				}
			}
		}
	}

	//return the list of moves
	return ret;
}

//isOver method - returns whether the game is over
bool GameState::isOver() const {
	//get the game-ending conditions
	bool noRed = this->getRedPower() < 1; //are there red pieces in play
	bool noBlue = this->getBluePower() < 1; //are there blue pieces
	bool noMoves = this->getMoves().length() < 1; //are there moves

	//and OR them together
	return (noRed || noBlue) xor noMoves;
}

//getRedPower method - returns the number of red pieces on the board
int GameState::getRedPower() const {
	//return the general power method for red
	return this->getGeneralPower(SpaceState::RED);
}

//getBluePower method - returns the number of blue pieces on the board
int GameState::getBluePower() const {
	//return the general power method for blue
	return this->getGeneralPower(SpaceState::BLUE);
}

//getEmptyPower method - returns the number of empty spaces on the board
int GameState::getEmptyPower() const {
	//return the the general power method for no piece
	return this->getGeneralPower(SpaceState::NONE);
}

//getCurrentPlayer method - returns the ID of the current player
PlayerID GameState::getCurrentPlayer() const {
	return this->curPlayer; //return the current player field
}

//getHumanPlayer method - returns the ID of the human player
PlayerID GameState::getHumanPlayer() const {
	return this->humanPlayer; //return the human player field
}

//isMoveLegal method - returns whether a given move is legal
bool GameState::isMoveLegal(const Move& m) const {
	return this->getMoves().contains(m); //return the move's legality
}

//getAdjacentEnemies method - returns a list of adjacent enemies
//for a given move
MoveList GameState::getAdjacentEnemies(const Move& m) const {
	int moveX = m.getX(); //localize the move's x-coord
	int moveY = m.getY(); //localize the move's y-coord

	MoveList ret; //the accumulated locations of the enemies

	//loop and get the 
	for(int x = (moveX - 1); x <= (moveX + 1); x++) {
		//handle out-of-bounds x-coords
		if((x < 1) || (x > BOARD_X)) { //if the x-coord is OOB
			continue; //then skip this loop
		}
		for(int y = (moveY - 1); y <= (moveY + 1); y++) {
			//handle out-of-bounds y-coords
			if((y < 1) || (y > BOARD_Y)) { //OOB y-coord
				continue; //skip the loop
			}

			//get a space state for the source move's ID
			SpaceState moveState = (m.getPlayerID()
						== PlayerID::RED) ?
						SpaceState::RED : 
						SpaceState::BLUE;

			//get the state of the current space being examined
			SpaceState curState = this->getStateAtCoords(x, y);

			//and compare the two states
			if((moveState != curState) && (curState != 
							SpaceState::NONE)) {
				//they match, so save the coordinates
				ret.push(Move(x, y, m.getPlayerID()));
			}

		}
	}

	return ret; //return the accumulated enemies
}

//public overloaded doMove method - executes a move
void GameState::doMove(const Move& m) {
	//get the list of adjacent enemies to the move
	MoveList enemies = this->getAdjacentEnemies(m);

	//execute the move
	this->doMove(m, true);

	//and replace the enemies with friendly pieces
	for(int i = 0; i < enemies.length(); i++) {
		this->doMove(enemies[i], false);
	}

}

//private overloaded doMove method - executes a move and updates the
//turn status
void GameState::doMove(const Move& m, bool flip) {
	//handle different players
	if(m.getPlayerID() == PlayerID::RED) { //red player's turn
		//place a red piece
		this->board.placeRedPiece(m.getX(), m.getY());
	} else { //blue player's turn
		//place a blue piece
		this->board.placeBluePiece(m.getX(), m.getY());
	}

	//and flip the current player ID if directed
	if(flip) {
		this->curPlayer = (this->curPlayer == PlayerID::RED) ?
					PlayerID::BLUE : PlayerID::RED;
	}
}

//update method - updates the game state
void GameState::update() {
	this->board.render(); //render the board
	cppcurses::reload(); //and update the terminal screen
}

//serialization operator
std::ostream& operator<<(std::ostream& os, const GameState& gs) {
	os << gs.board << '\n'; //serialize the board
	os << static_cast<int>(gs.curPlayer) << '\n'; //save the player ID
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, GameState& gs) {
	is >> gs.board; //deserialize the board
	int temp = 0; //used to read in the player ID
	is >> temp; //read in the player ID as an integer
	gs.curPlayer = static_cast<PlayerID>(temp); //assign the ID
	return is; //and return the stream
}

//private getPossMoves method - returns the possible moves for a given
//board space
MoveList GameState::getPossMoves(const CoordPair& center) const {
	//split the argument into x and y variables
	int x = center.getX();
	int y = center.getY();

	//declare the return list
	MoveList ret;

	//loop and assemble the possible moves
	for(int i = -1; i <= 1; i++) {
		//handle out-of-bounds search coords
		if(((i + x) < 1) || ((i + x) > BOARD_X)) { //OOB x-coord
			continue; //then skip it
		}
		for(int j = -1; j <= 1; j++) {
			//handle corners and the origin
			if(abs(i) == abs(j)) {
				continue; //skip the corners and center
			}

			//handle out-of-bounds search coords
			if(((j + y) < 1) || ((j + y) > BOARD_Y)) {
				continue; //skip OOB y-coords
			}

			//check if the space is empty
			if(this->getStateAtCoords(i + x, j + y) ==
							SpaceState::NONE) {

				//it is, so add that space as a move
				ret.push(Move(i + x, j + y, 
							this->curPlayer));
			}
		}
	}

	return ret; //return the assembled moves
}

//private getGeneralPower method - returns the total number of spaces with a
//specified state
int GameState::getGeneralPower(SpaceState testState) const {
	int total = 0; //the total number of spaces

	//loop and calculate the space total
	for(int x = 1; x <= BOARD_X; x++) {
		for(int y = 1; y <= BOARD_Y; y++) {
			//check for a matching state
			if(this->getStateAtCoords(x, y) == testState) {
				total++; //matching state, so inc the total
			}
		}
	}

	//return the total
	return total;
}

//end of implementation
