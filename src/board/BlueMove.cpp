/*
 * BlueMove.h
 * Declares a class that represents a blue Othello move
 * Created on 11/18/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the tebms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "BlueMove.h"

//first constructor
BlueMove::BlueMove(int newX, int newY)
	: Move(newX, newY, PlayerID::BLUE) //call superclass constructor
{
	//no code needed
}

//second constructor
BlueMove::BlueMove(const CoordPair& newCoords)
	: Move(newCoords, PlayerID::BLUE) //call superclass constructor
{
	//no code needed
}

//destructor
BlueMove::~BlueMove() {
	//no code needed
}

//copy constructor
BlueMove::BlueMove(const BlueMove& bm)
	: Move(bm) //call superclass copy constructor
{
	//no code needed
}

//move constructor
BlueMove::BlueMove(BlueMove&& bm)
	: Move(bm) //call superclass move constructor
{
	//no code needed
}

//assignment operator
BlueMove& BlueMove::operator=(const BlueMove& src) {
	Move::operator=(src); //call superclass assignment operator
	return *this; //and return the instance
}

//move operator
BlueMove& BlueMove::operator=(BlueMove&& src) {
	Move::operator=(src); //call superclass move operator
	return *this; //and return the instance
}

//serialization operator
std::ostream& operator<<(std::ostream& os, const BlueMove& bm) {
	os << static_cast<const Move&>(bm); //serialize the superclass
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, BlueMove& bm) {
	is >> static_cast<Move&>(bm); //deserialize the superclass
	return is; //and return the stream
}

//end of implementation
