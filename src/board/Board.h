/*
 * Board.h
 * Declares a class that represents an Othello board
 * Created on 11/18/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <cppcurses/cppcurses.h>
#include <cmath>
#include <vector>
#include "Move.h"
#include "Space.h"
#include "SpaceState.h"
#include "../player/PlayerID.h"
#include "../util/Constants.h"
#include "../util/CoordPair.h"
#include "Orient.h"
#include "../except/InvalidCoordsException.h"

//class declaration
class Board final {
	//public fields and methods
	public:
		//constructor
		explicit Board(Orient newOrient);

		//destructor
		~Board();

		//copy constructor
		Board(const Board& b);

		//move constructor
		Board(Board&& b);

		//assignment operator
		Board& operator=(const Board& src);

		//move operator
		Board& operator=(Board&& src);

		//getter methods
		
		//returns the orientation of the Board
		Orient getOrient() const;

		//returns the state of a given space on the Board
		//using coordinates
		SpaceState getStateAtCoords(int x, int y) const;

		//returns the state of a given space on the Board
		//using a CoordPair object
		SpaceState getStateAtCoords(const CoordPair& coords) const;

		//other methods
		
		//places a red piece at a given x and y location
		void placeRedPiece(int x, int y);

		//places a red piece at a given CoordPair location
		void placeRedPiece(const CoordPair& coords);

		//places a blue piece at a given x and y location
		void placeBluePiece(int x, int y);

		//places a blue piece at a given CoordPair location
		void placeBluePiece(const CoordPair& coords);

		//draws the board
		void render() const;

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const Board& b);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						Board& b);

	//private fields and methods
	private:
		//methods
		
		//frees the board memory
		void free();
		
		//verifies that a coordinate set is valid (on the board)
		void verifyCoords(int x, int y) const;

		//displays the row and column markers
		void displayMarkers() const;

		//fields
		std::vector<std::vector<Space>> data; //the board data
		Orient orient; //the orientation of the Board
		
};

//end of class
