/*
 * Move.h
 * Declares a class that represents an Othello move
 * Created on 11/17/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include "../player/PlayerID.h"
#include "../util/CoordPair.h"
#include "../except/InvalidCoordsException.h"
#include "../util/Constants.h"

//class declaration
class Move {
	//public fields and methods
	public:
		//default constructor
		Move();

		//constructor 1 - constructs from x and y coordinates
		Move(int newX, int newY, PlayerID newID);

		//constructor 2 - constructs from a CoordPair
		Move(const CoordPair& newPair, PlayerID newID);

		//destructor
		virtual ~Move();

		//copy constructor
		Move(const Move& m);

		//move constructor
		Move(Move&& m);

		//assignment operator
		Move& operator=(const Move& src);

		//move operator
		Move& operator=(Move&& src);

		//equality operator
		bool operator==(const Move& other) const;

		//inequality operator
		bool operator!=(const Move& other) const;

		//getter methods
		
		//returns the x-coord of the Move
		int getX() const;

		//returns the y-coord of the Move
		int getY() const;

		//returns the ID of the player that is making the Move
		PlayerID getPlayerID() const;

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const Move& m);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						Move& m);

	//protected fields and methods
	protected:
		//fields
		CoordPair coords; //the coordinates of the Move
		PlayerID playerID; //the ID of the player making the move
};

//end of class
