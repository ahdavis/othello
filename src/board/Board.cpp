/*
 * Board.cpp
 * Implements a class that represents an Othello board
 * Created on 11/18/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Board.h"

//constructor
Board::Board(Orient newOrient)
	: data(BOARD_X, std::vector<Space>(BOARD_Y)), orient(newOrient)
{
	//initialize the data vector
	for(int x = 0; x < BOARD_X; x++) {
		for(int y = 0; y < BOARD_Y; y++) {
			//add a space to the vector
			this->data[x][y] = Space(x + 1, y + 1);
		}
	}

	//initialize the four corners based on the Orient parameter
	if(this->orient == Orient::RED_F) { //red in front
		this->data[0][0].placeBluePiece(); //place the first blue
		this->data[BOARD_X - 1][0].placeBluePiece(); //second
		this->data[0][BOARD_Y - 1].placeRedPiece(); //first red
		this->data[BOARD_X - 1][BOARD_Y - 1].placeRedPiece(); 
	} else if(this->orient == Orient::BLUE_F) { //blue in front
		this->data[0][0].placeRedPiece(); //place the first red
		this->data[BOARD_X - 1][0].placeRedPiece(); //second
		this->data[0][BOARD_Y - 1].placeBluePiece(); //first blue
		this->data[BOARD_X - 1][BOARD_Y - 1].placeBluePiece(); 
	} else if(this->orient == Orient::RED_L) { //red in lower left
		this->data[0][0].placeBluePiece(); //place the first blue
		this->data[BOARD_X - 1][0].placeRedPiece(); //first red
		this->data[0][BOARD_Y - 1].placeRedPiece(); //second red
		this->data[BOARD_X - 1][BOARD_Y - 1].placeBluePiece(); 
	} else if(this->orient == Orient::BLUE_L) { //blue in lower left
		this->data[0][0].placeRedPiece(); //place the first red
		this->data[BOARD_X - 1][0].placeBluePiece(); //first blue
		this->data[0][BOARD_Y - 1].placeBluePiece(); //second blue
		this->data[BOARD_X - 1][BOARD_Y - 1].placeRedPiece(); 
	}

}

//destructor
Board::~Board() {
	//free the board memory
	this->free();
}

//copy constructor
Board::Board(const Board& b)
	: data(b.data), orient(b.orient) //copy the fields
{
	//no code needed
}

//move constructor
Board::Board(Board&& b)
	: data(b.data), orient(b.orient) //move the fields
{
	//free the temporary board's board memory
	b.free();
}

//assignment operator
Board& Board::operator=(const Board& src) {
	this->data = src.data; //assign the data
	this->orient = src.orient; //assign the orientation
	return *this; //and return the object
}

//move operator
Board& Board::operator=(Board&& src) {
	this->data = src.data; //move the data
	this->orient = src.orient; //move the orientation
	src.free(); //free the temporary memory
	return *this; //and return the object
}

//getOrient method - returns the orientation of the Board
Orient Board::getOrient() const {
	return this->orient; //return the orient field
}

//first getStateAtCoords method - returns the state of a given space
//on the Board using coordinates
SpaceState Board::getStateAtCoords(int x, int y) const {
	this->verifyCoords(x, y); //verify the coordinates
	return this->data[x - 1][y - 1].getState(); //and return the state
}

//second getStateAtCoords method - returns the state of a given space
//on the Board using a CoordPair object
SpaceState Board::getStateAtCoords(const CoordPair& coords) const {
	//call the other method
	return this->getStateAtCoords(coords.getX(), coords.getY());
}

//first placeRedPiece method - places a red piece at a given x and y 
//location
void Board::placeRedPiece(int x, int y) {
	this->verifyCoords(x, y); //verify the coordinates
	this->data[x - 1][y - 1].placeRedPiece(); //and place the piece
}

//second placeRedPiece method - places a red piece at a given CoordPair
//location
void Board::placeRedPiece(const CoordPair& coords) {
	//call the other method
	this->placeRedPiece(coords.getX(), coords.getY());
}

//first placeBluePiece method - places a blue piece at a given x and y 
//location
void Board::placeBluePiece(int x, int y) {
	this->verifyCoords(x, y); //verify the coordinates
	this->data[x - 1][y - 1].placeBluePiece(); //and place the piece
}

//second placeBluePiece method - places a blue piece at a given CoordPair
//location
void Board::placeBluePiece(const CoordPair& coords) {
	//call the other method
	this->placeBluePiece(coords.getX(), coords.getY());
}

//render method - draws the board
void Board::render() const {
	//loop and render the board
	for(int x = 0; x < BOARD_X; x++) {
		for(int y = 0; y < BOARD_Y; y++) {
			this->data[x][y].render(); //render each space
		}
	}

	//and draw the markers
	this->displayMarkers();

}

//serialization operator
std::ostream& operator<<(std::ostream& os, const Board& b) {
	//loop and serialize the board data
	for(int x = 0; x < BOARD_X; x++) {
		for(int y = 0; y < BOARD_Y; y++) {
			//serialize each Space in the Board
			os << b.data[x][y] << '\n';
		}
	}
	
	//serialize the board orientation
	os << static_cast<int>(b.orient) << '\n';

	//and return the stream
	return os;
}

//deserialization operator
std::istream& operator>>(std::istream& is, Board& b) {
	//loop and deserialize the board data
	for(int x = 0; x < BOARD_X; x++) {
		for(int y = 0; y < BOARD_Y; y++) {
			//deserialize each space in the board
			is >> b.data[x][y];
		}
	}

	//deserialize the board orientation
	int temp = 0; 
	is >> temp; //load the orientation as an integer
	b.orient = static_cast<Orient>(temp); //and cast it over

	//and return the stream
	return is;
}

//private free method - deallocates the board memory
void Board::free() {
	//loop and clear each board row
	for(auto& row : this->data) {
		row.clear(); //clear each row
	}

	//and clear the columns
	this->data.clear();
}

//private verifyCoords method - verifies that a coordinate set is valid
//(on the board)
void Board::verifyCoords(int x, int y) const {
	if(((x <= 0) || (x > BOARD_X)) ||
		((y <= 0) || (y > BOARD_Y))) { //if the coords are invalid
		throw InvalidCoordsException(x, y); //then throw an error
	}
}

//private displayMarkers method - draws the row and column markers
void Board::displayMarkers() const {
	//get the drawing coordinates
	int drawX = BOARD_OFS_X;
	int drawY = BOARD_OFS_Y;

	//enable green text
	cppcurses::color_pair mark = 
		cppcurses::color_pair(cppcurses::color_green(),
					cppcurses::color_black());
	mark.enable();

	//get a movable stream to draw the markers
	cppcurses::omovablestream oms;

	//set the coordinates of the stream
	oms.X(drawX);
	oms.Y(drawY);

	//draw the starting symbol
	oms.putchar(cppcurses::acs::bullet());

	//loop and draw the x-axis markers
	for(int x = 0; x < BOARD_X; x++) {
		//adjust the drawing coordinates
		drawX += SPACE_DIM - floor(SPACE_DIM / 2);
		oms.X(drawX);
		oms.Y(drawY);

		//and draw the marker
		oms.putchar((x + 1) + '0');
	}

	//reset the x-coord to the origin
	drawX = BOARD_OFS_X;
	oms.X(drawX);

	//loop and draw the y-axis markers
	for(int y = 0; y < BOARD_Y; y++) {
		//adjust the drawing coordinates
		drawY += SPACE_DIM - floor(SPACE_DIM / 2);
		oms.Y(drawY);
		oms.X(drawX);

		//and draw the marker
		oms.putchar((y + 1) + '0');
	}

	//and disable the color pair
	mark.disable();

}

//end of implementation
