/*
 * Move.cpp
 * Implements a class that represents an Othello move
 * Created on 11/17/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Move.h"

//default constructor
Move::Move()
	: Move(1, 1, PlayerID::RED) //call the first constructor
{
	//no code needed
}

//first constructor
Move::Move(int newX, int newY, PlayerID newID)
	: coords(newX, newY), playerID(newID) //init the fields
{
	//ensure that the coordinates are not invalid
	if((newX < 1) || (newX > BOARD_X)) { //if the x-coordinate is bad
		//then throw an exception
		throw InvalidCoordsException(newX, newY);
	}

	if((newY < 1) || (newY > BOARD_Y)) { //if the y-coordinate is bad
		//then throw an exception
		throw InvalidCoordsException(newX, newY);
	}
}

//second constructor
Move::Move(const CoordPair& newCoords, PlayerID newID)
	: Move(newCoords.getX(), newCoords.getY(), newID)
{
	//no code needed
}

//destructor
Move::~Move() {
	//no code needed
}

//copy constructor
Move::Move(const Move& m)
	: coords(m.coords), playerID(m.playerID) //copy the fields
{
	//no code needed
}

//move constructor
Move::Move(Move&& m)
	: coords(m.coords), playerID(m.playerID) //move the fields
{
	//no code needed
}

//assignment operator
Move& Move::operator=(const Move& src) {
	this->coords = src.coords; //assign the coordinates
	this->playerID = src.playerID; //assign the player ID
	return *this; //and return the object
}

//move operator
Move& Move::operator=(Move&& src) {
	this->coords = src.coords; //move the coordinates
	this->playerID = src.playerID; //move the player ID
	return *this; //and return the object
}

//equality operator
bool Move::operator==(const Move& other) const {
	return (this->coords == other.coords) && 
		(this->playerID == other.playerID); //compare the objects
}

//inequality operator
bool Move::operator!=(const Move& other) const {
	bool cmp = (*this == other); //compare this object and the other
	return !cmp; //and return its negation
}

//getX method - returns the x-component of the Move
int Move::getX() const {
	return this->coords.getX(); //return the x-coord
}

//getY method - returns the y-component of the Move
int Move::getY() const {
	return this->coords.getY(); //return the y-coord
}

//getPlayerID method - returns the player ID of the Move
PlayerID Move::getPlayerID() const {
	return this->playerID; //return the player ID
}

//serialization operator
std::ostream& operator<<(std::ostream& os, const Move& m) {
	os << m.coords << '\n'; //serialize the coords
	os << static_cast<int>(m.playerID) << '\n'; //serialize the ID
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, Move& m) {
	is >> m.coords; //deserialize the coords
	int temp = 0; //used to read in the ID
	is >> temp; //read in the ID as an integer
	m.playerID = static_cast<PlayerID>(temp); //assign the ID
	return is; //and return the stream
}

//end of implementation
