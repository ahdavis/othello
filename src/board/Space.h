/*
 * Space.h
 * Declares a class that represents an Othello board space
 * Created on 11/12/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "../util/Constants.h"
#include "../util/CoordPair.h"
#include <iostream>
#include <cmath>
#include "../piece/Piece.h"
#include "../piece/RedPiece.h"
#include "../piece/BluePiece.h"
#include "SpaceState.h"
#include <cppcurses/cppcurses.h>

//class declaration
class Space final {
	//public fields and methods
	public:
		//default constructor
		Space();

		//constructor
		Space(int newX, int newY);

		//destructor
		~Space();

		//copy constructor
		Space(const Space& s);

		//move constructor
		Space(Space&& s);

		//assignment operator
		Space& operator=(const Space& src);

		//move operator
		Space& operator=(Space&& src);

		//getter method
		
		//returns the state of the Space
		SpaceState getState() const;

		//other methods
		
		//places a red piece on the Space
		void placeRedPiece();

		//places a blue piece on the Space
		void placeBluePiece();

		//removes a piece from the Space
		void removePiece();

		//renders the Space
		void render() const;

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const Space& s);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						Space& s);

	//private fields and methods
	private:
		//method
		void free(); //deallocates the Space

		//fields
		
		//coordinate field
		CoordPair cen; //the center of the Space

		//index fields
		int x; //the x-index of the Space
		int y; //the y-index of the Space

		//other fields
		Piece* piece; //the piece that is on the Space
		SpaceState state; //the state of the Space

};

//end of class
