/*
 * RedMove.h
 * Declares a class that represents a red Othello move
 * Created on 11/18/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "RedMove.h"

//first constructor
RedMove::RedMove(int newX, int newY)
	: Move(newX, newY, PlayerID::RED) //call superclass constructor
{
	//no code needed
}

//second constructor
RedMove::RedMove(const CoordPair& newCoords)
	: Move(newCoords, PlayerID::RED) //call superclass constructor
{
	//no code needed
}

//destructor
RedMove::~RedMove() {
	//no code needed
}

//copy constructor
RedMove::RedMove(const RedMove& rm)
	: Move(rm) //call superclass copy constructor
{
	//no code needed
}

//move constructor
RedMove::RedMove(RedMove&& rm)
	: Move(rm) //call superclass move constructor
{
	//no code needed
}

//assignment operator
RedMove& RedMove::operator=(const RedMove& src) {
	Move::operator=(src); //call superclass assignment operator
	return *this; //and return the instance
}

//move operator
RedMove& RedMove::operator=(RedMove&& src) {
	Move::operator=(src); //call superclass move operator
	return *this; //and return the instance
}

//serialization operator
std::ostream& operator<<(std::ostream& os, const RedMove& rm) {
	os << static_cast<const Move&>(rm); //serialize the superclass
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, RedMove& rm) {
	is >> static_cast<Move&>(rm); //deserialize the superclass
	return is; //and return the stream
}

//end of implementation
