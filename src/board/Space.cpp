/*
 * Space.cpp
 * Implements a class that represents an Othello board space
 * Created on 11/13/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Space.h"

//default constructor
Space::Space()
	: Space(0, 0) //call other constructor
{
	//no code needed
}

//constructor
Space::Space(int newX, int newY)
	: cen(), x(0), y(0), piece(nullptr), state(SpaceState::NONE)
{
	//calculate the center coordinates
	
	//get the offset of the center of the space
	int cenXOfs = floor(SPACE_DIM / 2);
	int cenYOfs = floor(SPACE_DIM / 2);

	//calculate the coordinates of the top left corner of the space
	this->x = ((newX - 1) * SPACE_DIM) + 1;
	this->y = ((newY - 1) * SPACE_DIM) + 1;

	//pretty up the board by offsetting the coordinates
	if(newX > 1) { //if the space is not on the edge
		this->x = this->x - (newX - 1);
	}

	if(newY > 1) { //if the space is not on the edge
		this->y = this->y - (newY - 1);
	}

	//add the board offsets
	this->x += BOARD_OFS_X;
	this->y += BOARD_OFS_Y;

	//and calculate the center coordinates
	cen.setX(cenXOfs + this->x);
	cen.setY(cenYOfs + this->y);
}

//destructor
Space::~Space() {
	//free the space
	this->free();
}

//copy constructor
Space::Space(const Space& s)
	: cen(s.cen), x(s.x), y(s.y), piece(nullptr), state(s.state)
{
	//copy the piece pointer
	if(s.piece != nullptr) { //if the other space has a piece on it
		this->piece = new Piece(*s.piece); //then copy it
	}
}

//move constructor
Space::Space(Space&& s)
	: cen(s.cen), x(s.x), y(s.y), piece(nullptr), state(s.state)
{
	//move the piece pointer
	if(s.piece != nullptr) { //if the other space has a piece on it
		this->piece = new Piece(*s.piece); //then move it
	}

	//and free the temporary object
	s.free();
}

//assignment operator
Space& Space::operator=(const Space& src) {
	//assign the fields
	this->cen = src.cen; //assign the center coordinates
	this->x = src.x; //assign the x index
	this->y = src.y; //assign the y index
	this->state = src.state; //assign the space state

	//assign the piece pointer
	this->free(); //deallocate the current piece pointer
	if(src.piece != nullptr) { //if the other space has a piece on it
		this->piece = new Piece(*src.piece); //then assign it
	}

	//and return the object
	return *this;
}

//move operator
Space& Space::operator=(Space&& src) {
	//assign the fields
	this->cen = src.cen; //move the center coordinates
	this->x = src.x; //move the x index
	this->y = src.y; //move the y index
	this->state = src.state; //move the space state

	//move the piece pointer
	this->free(); //deallocate the current piece pointer
	if(src.piece != nullptr) { //if the other space has a piece on it
		this->piece = new Piece(*src.piece); //then move it
	}

	//deallocate the other space
	src.free();

	//and return the object
	return *this;
}

//getState method - returns the state of the Space
SpaceState Space::getState() const {
	return this->state;
}

//placeRedPiece method - places a red piece on the Space
void Space::placeRedPiece() {
	//deallocate the current piece
	if(this->piece != nullptr) { //if there is already a piece
		this->free(); //then deallocate it
	}

	//change the state field to a red piece state
	this->state = SpaceState::RED;

	//and place a red piece on the space
	this->piece = new RedPiece(this->cen);
}

//placeBluePiece method - places a blue piece on the Space
void Space::placeBluePiece() {
	//deallocate the current piece
	if(this->piece != nullptr) { //if there is already a piece
		this->free(); //then deallocate it
	}

	//change the state field to a blue piece state
	this->state = SpaceState::BLUE;

	//and place a blue piece on the space
	this->piece = new BluePiece(this->cen);
}

//removePiece method - removes a piece from the Space
void Space::removePiece() {
	//deallocate the current piece
	if(this->piece != nullptr) { //if there is already a piece
		this->free(); //then deallocate it
	}

	//and update the space field
	this->state = SpaceState::NONE;
}

//render method - draws the Space on the screen
void Space::render() const {
	//create a movable stream to render the space with
	cppcurses::omovablestream oms;

	//initialize the drawing coordinates
	int drawX = this->x; 
	int drawY = this->y;

	//draw the upper left corner
	oms.X(drawX);
	oms.Y(drawY);
	oms.putchar('+');

	//draw the top side
	for(int i = 0; i < SPACE_DIM - 1; i++) {
		drawX += 1; //increment the draw coordinate
		oms.X(drawX); //set the x-coord
		oms.Y(drawY); //set the y-coord
		oms.putchar('-'); //and draw the symbol
	}

	//draw the upper right corner
	oms.X(drawX);
	oms.Y(drawY);
	oms.putchar('+');

	//draw the right side
	for(int j = 0; j < SPACE_DIM - 1; j++) {
		drawY += 1; //increment the draw coordinate
		oms.X(drawX); //set the x-coord
		oms.Y(drawY); //set the y-coord
		oms.putchar('|'); //and draw the symbol
	}

	//draw the lower right corner
	oms.X(drawX);
	oms.Y(drawY);
	oms.putchar('+');

	//draw the bottom side
	for(int k = 0; k < SPACE_DIM - 1; k++) {
		drawX -= 1; //increment the draw coordinate
		oms.X(drawX); //set the x-coord
		oms.Y(drawY); //set the y-coord
		oms.putchar('-'); //and draw the symbol
	}

	//draw the lower left corner
	oms.X(drawX);
	oms.Y(drawY);
	oms.putchar('+');

	//draw the left side
	for(int l = 0; l < SPACE_DIM - 1; l++) {
		drawY -= 1; //increment the draw coordinate
		oms.X(drawX); //set the x-coord
		oms.Y(drawY); //set the y-coord
		oms.putchar('|'); //and draw the symbol
	}

	//draw the upper left coordinate
	oms.X(drawX);
	oms.Y(drawY);
	oms.putchar('+');

	//and draw the Piece
	if(this->piece != nullptr) { //if the piece exists
		this->piece->draw(); //then draw it
	}

}

//serialization operator
std::ostream& operator<<(std::ostream& os, const Space& s) {
	os << s.cen << '\n'; //serialize the center coordinates
	os << s.x << '\n'; //serialize the x index
	os << s.y << '\n'; //serialize the y index
	if(s.piece != nullptr) { //if the piece exists
		os << true << '\n'; //then store a true flag
	} else {
		os << false << '\n'; //then store a false flag
	}
	if(s.piece != nullptr) { //if the piece exists	
		os << *s.piece << '\n'; //then serialize the piece
	}
	os << static_cast<int>(s.state); //serialize the state
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, Space& s) {
	is >> s.cen; //deserialize the center coordinates
	is >> s.x; //deserialize the x index
	is >> s.y; //deserialize the y index
	bool flag; //used to determine whether a piece was stored
	is >> flag; //read in the piece flag
	s.free(); //deallocate the space
	if(flag) { //if a piece was stored
		s.piece = new RedPiece(0, 0); //then allocate the piece
		is >> *s.piece; //and read it in
	}
	int tempState = 0; //used to read in the state field
	is >> tempState; //read in the state field
	s.state = static_cast<SpaceState>(tempState); //and assign it
	return is; //and return the stream
}


//private free method - deallocates the memory associated with the Space
void Space::free() {
	delete this->piece; //deallocate the piece field
	this->piece = nullptr; //zero it out
	this->state = SpaceState::NONE; //and update the state field
}

//end of implementation
