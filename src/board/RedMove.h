/*
 * RedMove.h
 * Declares a class that represents a red Othello move
 * Created on 11/18/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include "../util/CoordPair.h"
#include "../player/PlayerID.h"
#include "Move.h"

//class declaration
class RedMove final : public Move
{
	//public fields and methods
	public:
		//first constructor
		RedMove(int newX, int newY);

		//second constructor
		explicit RedMove(const CoordPair& newCoords);

		//destructor
		~RedMove();

		//copy constructor
		RedMove(const RedMove& rm);

		//move constructor
		RedMove(RedMove&& rm);

		//assignment operator
		RedMove& operator=(const RedMove& src);

		//move operator
		RedMove& operator=(RedMove&& src);

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const RedMove& rm);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						RedMove& rm);

	//no private members
	
};

//end of class
