/*
 * BlueMove.h
 * Declares a class that represents a blue Othello move
 * Created on 11/18/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the tebms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include "../util/CoordPair.h"
#include "../player/PlayerID.h"
#include "Move.h"

//class declaration
class BlueMove final : public Move
{
	//public fields and methods
	public:
		//first constructor
		BlueMove(int newX, int newY);

		//second constructor
		explicit BlueMove(const CoordPair& newCoords);

		//destructor
		~BlueMove();

		//copy constructor
		BlueMove(const BlueMove& bm);

		//move constructor
		BlueMove(BlueMove&& bm);

		//assignment operator
		BlueMove& operator=(const BlueMove& src);

		//move operator
		BlueMove& operator=(BlueMove&& src);

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const BlueMove& bm);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						BlueMove& bm);

	//no private members
	
};

//end of class
