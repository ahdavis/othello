/*
 * Constants.cpp
 * Defines constants for Othello
 * Created on 11/12/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "Constants.h"

//define board and space constants
const int BOARD_X = 7; //the x-dimension of a board (MUST BE A SINGLE DIGIT)
const int BOARD_Y = 7; //the y-dimension of a board (MUST BE A SINGLE DIGIT)
const int BOARD_OFS_X = 2; //the x-offset of a board
const int BOARD_OFS_Y = 1; //the y-offset of a board
const int SPACE_DIM = 3; //the dimensions of a board space (MUST BE ODD)

//define AI difficulty constants
const int EASY_DEPTH = 9; //the AI lookahead depth in easy mode
const int MED_DEPTH = 10; //the AI lookahead depth in medium mode
const int HARD_DEPTH = 11; //the AI lookahead depth in hard mode

//define interface constants
const int INFO_OFS_X = 3; //the info box's X offset relative to the board
const int INFO_OFS_Y = 4; //the info box's Y offset relative to the top
const int INFO_DIM_X = 7; //the info box's x-dimension
const int INFO_DIM_Y = 3; //the info box's y-dimension
const int CMD_OFS_X = 2; //the command bar's X offset relative to the side
const int CMD_OFS_Y = 2; //the command bar's Y offset relative to the board
const int CMD_DIM_X = 30; //the command bar's x-dimension
const int CMD_DIM_Y = 1; //the command bar's y-dimension

//end of definitions
