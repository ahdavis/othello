/*
 * fileExists.h
 * Declares a function that returns whether a file exists
 * Created on 11/26/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <cstdio>


//function declarations

//Returns whether a given file exists. The filename is supplied by a
//C string.
bool fileExists(const char* fileName);

//Returns whether a given file exists. The filename is supplied by a
//std::string.
bool fileExists(const std::string& fileName);

//end of functions
