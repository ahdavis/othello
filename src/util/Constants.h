/*
 * Constants.h
 * Declares constants for Othello
 * Created on 11/12/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//no includes

//board and space constants
extern const int BOARD_X; //the x-dimension of the board
extern const int BOARD_Y; //the y-dimension of the board
extern const int BOARD_OFS_X; //the x-offset of the board
extern const int BOARD_OFS_Y; //the y-offset of the board
extern const int SPACE_DIM; //the dimension of a board space (MUST BE ODD)

//AI difficulty constants
extern const int EASY_DEPTH; //the depth that the AI looks ahead in easy
extern const int MED_DEPTH; //the depth that the AI looks ahead in medium
extern const int HARD_DEPTH; //the depth that the AI looks ahead in hard

//interface constants
extern const int INFO_OFS_X; //the x-offset of the info box (from the board)
extern const int INFO_OFS_Y; //the y-offset of the info box (from the top)
extern const int INFO_DIM_X; //the x-dimension of the info box
extern const int INFO_DIM_Y; //the y-dimension of the info box
extern const int CMD_OFS_X; //the x-offset of the command bar (rel. board)
extern const int CMD_OFS_Y; //the y-offset of the command bar (rel. board)
extern const int CMD_DIM_X; //the x-dimension of the command bar
extern const int CMD_DIM_Y; //the y-dimension of the command bar

//end of constants
