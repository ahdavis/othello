/*
 * CoordPair.cpp
 * Implements a class that represents a coordinate pair
 * Created on 11/12/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "CoordPair.h"

//first constructor
CoordPair::CoordPair(int newX, int newY)
	: x(newX), y(newY) //init the fields
{
	//no code needed
}

//second constructor
CoordPair::CoordPair()
	: CoordPair(0, 0) //call the other constructor
{
	//no code needed
}

//destructor
CoordPair::~CoordPair() {
	//no code needed
}

//copy constructor
CoordPair::CoordPair(const CoordPair& cp)
	: x(cp.x), y(cp.y) //copy the fields
{
	//no code needed
}

//move constructor
CoordPair::CoordPair(CoordPair&& cp)
	: x(cp.x), y(cp.y) //move the fields
{
	//no code needed
}

//assignment operator
CoordPair& CoordPair::operator=(const CoordPair& src) {
	this->x = src.x; //assign the x-component
	this->y = src.y; //assign the y-component
	return *this; //and return the object
}

//move operator
CoordPair& CoordPair::operator=(CoordPair&& src) {
	this->x = src.x; //move the x-component
	this->y = src.y; //move the y-component
	return *this; //and return the object
}

//equality operator
bool CoordPair::operator==(const CoordPair& other) const {
	//compare the coordinates
	if((this->x == other.x) && (this->y == other.y)) {
		return true; //the coordinates match
	} else {
		return false; //the coordinates don't match
	}
}

//inequality operator
bool CoordPair::operator!=(const CoordPair& other) const {
	//get a comparison of the coordinates from the equality operator
	bool cmp = *this == other;

	//and return its negation
	return !cmp;
}

//getX method - returns the x-component of the CoordPair
int CoordPair::getX() const {
	return this->x; //return the x-component
}

//getY method - returns the y-component of the CoordPair
int CoordPair::getY() const {
	return this->y; //return the y-component
}

//setX method - sets the x-component of the CoordPair
void CoordPair::setX(int newX) {
	this->x = newX; //set the x-component
}

//setY method - sets the y-component of the CoordPair
void CoordPair::setY(int newY) {
	this->y = newY; //set the y-component
}

//serialization operator
std::ostream& operator<<(std::ostream& os, const CoordPair& cp) {
	os << cp.x << '\n'; //serialize the x-component
	os << cp.y << '\n'; //serialize the y-component
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, CoordPair& cp) {
	is >> cp.x; //deserialize the x-component
	is >> cp.y; //deserialize the y-component
	return is; //and return the stream
}

//end of implementation
