/*
 * fileExists.cpp
 * Implements a function that returns whether a file exists
 * Created on 11/26/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "fileExists.h"

//first fileExists function - returns whether a file specified by a
//C string exists
bool fileExists(const char* fileName) {
	FILE* test = fopen(fileName, "r"); //attempt to open the file

	//determine whether the file exists
	//if the test variable is not null,
	//then the file exists
	if(test) { //if the file exists
		fclose(test); //then close it
		return true; //and return true
	} else { //if the file does not exist
		return false; //then return false
	}
}

//second fileExists function - returns whether a file specified by a
//std::string exists
bool fileExists(const std::string& fileName) {
	//call the other fileExists function
	return fileExists(fileName.c_str());
}

//end of implementation
