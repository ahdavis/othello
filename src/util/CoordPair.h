/*
 * CoordPair.h
 * Declares a class that represents a coordinate pair
 * Created on 11/12/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <iostream>

//class declaration
class CoordPair final {
	//public fields and methods
	public:
		//first constructor - constructs from two coordinates
		CoordPair(int newX, int newY);

		//second constructor - constructs with two zeroes
		CoordPair();

		//destructor
		~CoordPair();

		//copy constructor
		CoordPair(const CoordPair& cp);

		//move constructor
		CoordPair(CoordPair&& cp);

		//assignment operator
		CoordPair& operator=(const CoordPair& src);

		//move operator
		CoordPair& operator=(CoordPair&& src);

		//equality operator
		bool operator==(const CoordPair& other) const;

		//inequality operator
		bool operator!=(const CoordPair& other) const;

		//getter methods
		
		//returns the x-component of the CoordPair
		int getX() const;

		//returns the y-component of the CoordPair
		int getY() const;

		//setter methods
		
		//sets the x-component of the CoordPair
		void setX(int newX);

		//sets the y-component of the CoordPair
		void setY(int newY);

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os,
						const CoordPair& cp);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						CoordPair& cp);

	//private fields and methods
	private:
		//fields
		int x; //the x-component of the CoordPair
		int y; //the y-component of the CoordPair
};

//end of class
