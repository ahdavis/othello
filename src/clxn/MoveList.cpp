/*
 * MoveList.cpp
 * Implements a class that represents a list of Othello moves
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "MoveList.h"

//constructor
MoveList::MoveList()
	: data() //init the field
{
	//no code needed
}

//destructor
MoveList::~MoveList() {
	//clear the list
	this->clear();
}

//copy constructor
MoveList::MoveList(const MoveList& ml)
	: data(ml.data) //copy the data
{
	//no code needed
}

//move constructor
MoveList::MoveList(MoveList&& ml)
	: data(ml.data) //move the data
{
	//and clear the temporary
	ml.clear();
}

//assignment operator
MoveList& MoveList::operator=(const MoveList& src) {
	this->data = src.data; //assign the data field
	return *this; //and return the object
}

//move operator
MoveList& MoveList::operator=(MoveList&& src) {
	this->data = src.data; //move the data field
	src.clear(); //clear the temporary
	return *this; //and return the object
}

//subscript operator
Move& MoveList::operator[](int index) {
	//verify that the index is valid
	if((index < 0) || (index >= this->length())) { //if it is not valid
		throw IndexException(index); //then throw an exception
	}

	//it is valid, so return the move at that index
	return this->data[index];
}

//length method - returns the length of the list
int MoveList::length() const {
	return this->data.size();
}

//push method - adds a Move to the list
void MoveList::push(const Move& m) {
	this->data.push_back(m); //add the move to the list
}

//pop method - removes the top Move from the list and returns it
Move MoveList::pop() {
	//get the top move in the list
	Move ret = this->data.back();

	//remove the top move
	this->data.pop_back();

	//and return the former top move
	return ret;
}

//removeAt method - removes a move at a specific index from the list
//and returns it
Move MoveList::removeAt(int index) {
	//verify that the index is not invalid
	if((index < 0) || (index >= this->length())) { //if it is invalid
		throw IndexException(index); //then throw an exception
	}

	//the index is valid
	Move ret = this->data[index]; //save the move at the index
	this->data.erase(this->data.begin() + index); //remove that move
	return ret; //and return the saved move
}

//clear method - clears the list
void MoveList::clear() {
	this->data.clear(); //clear the inner vector
}

//contains method - returns whether a move is in the list
bool MoveList::contains(const Move& m) const {
	for(const Move& move : this->data) { //loop through the data
		if(move == m) { //if the moves are equal
			return true; //then return true
		}
	}
	//no match, so return false
	return false;
}

//serialization operator
std::ostream& operator<<(std::ostream& os, const MoveList& ml) {
	os << ml.length() << '\n'; //serialize the length as a sentinel
	for(const Move& m : ml.data) { //loop through the move vector
		os << m << '\n'; //and serialize each move
	}
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, MoveList& ml) {
	int tempLen = 0; //used to read in the length
	is >> tempLen; //read in the saved length
	for(int i = 0; i < tempLen; i++) { //loop over the saved length
		Move tempMove; //used to read in the saved moves
		is >> tempMove; //read in each saved move
		ml.push(tempMove); //and add the move to the list
	}
	return is; //and return the stream
}

//end of implementation
