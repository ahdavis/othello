/*
 * ScoreList.cpp
 * Implements a class that represents a list of Othello scores
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include header
#include "ScoreList.h"

//constructor
ScoreList::ScoreList()
	: data() //init the field
{
	//no code needed
}

//destructor
ScoreList::~ScoreList() {
	//clear the list
	this->clear();
}

//copy constructor
ScoreList::ScoreList(const ScoreList& sl)
	: data(sl.data) //copy the data
{
	//no code needed
}

//move constructor
ScoreList::ScoreList(ScoreList&& sl)
	: data(sl.data) //move the data
{
	//and clear the temporary
	sl.clear();
}

//assignment operator
ScoreList& ScoreList::operator=(const ScoreList& src) {
	this->data = src.data; //assign the data field
	return *this; //and return the object
}

//move operator
ScoreList& ScoreList::operator=(ScoreList&& src) {
	this->data = src.data; //move the data field
	src.clear(); //clear the temporary
	return *this; //and return the object
}

//subscript operator
int& ScoreList::operator[](int index) {
	//verify that the index is valid
	if((index < 0) || (index >= this->length())) { //if it's not valid
		throw IndexException(index); //then throw an exception
	}

	//it is valid, so return the move at that index
	return this->data[index];
}

//length method - returns the length of the list
int ScoreList::length() const {
	return this->data.size();
}

//push method - adds a Move to the list
void ScoreList::push(int s) {
	this->data.push_back(s); //add the score to the list
}

//pop method - removes the top Move from the list and returns it
int ScoreList::pop() {
	//get the top score in the list
	int ret = this->data.back();

	//remove the top score
	this->data.pop_back();

	//and return the former top score
	return ret;
}

//removeAt method - removes a move at a specific index from the list
//and returns it
int ScoreList::removeAt(int index) {
	//verify that the index is not invalid
	if((index < 0) || (index >= this->length())) { //if it is invalid
		throw IndexException(index); //then throw an exception
	}

	//the index is valid
	int ret = this->data[index]; //save the score at the index
	this->data.erase(this->data.begin() + index); //remove that score
	return ret; //and return the saved score
}

//clear method - clears the list
void ScoreList::clear() {
	this->data.clear(); //clear the inner vector
}

//maxIndex method - returns the index of the maximum score in the list
//returns -1 if the list is empty
int ScoreList::maxIndex() const {
	//handle an empty list
	if(this->length() <= 0) { //if the list is empty
		return -1; //then return -1
	}

	//get the maximum value
	int max = *std::max_element(this->data.begin(), this->data.end());

	//and loop to get the index
	for(int i = 0; i < this->length(); i++) {
		if(this->data[i] == max) { //if the element is found
			return i; //then return its index
		}
	}

	return -1; //return a default value
}

//minIndex method - returns the index of the minimum score in the list
//returns -1 if the list is empty
int ScoreList::minIndex() const {
	//handle an empty list
	if(this->length() <= 0) { //if the list is empty
		return -1; //then return -1
	}

	//get the minimum element
	int min = *std::min_element(this->data.begin(), this->data.end());

	//and loop to get the index
	for(int i = 0; i < this->length(); i++) {
		if(this->data[i] == min) { //if the element is found
			return i; //then return the index
		}
	}

	return -1; //return a default value
}

//indexOf method - returns the index of a given item in the list
//returns -1 if the item is not found
int ScoreList::indexOf(int item) const {
	//loop and find the item
	for(int i = 0; i < this->length(); i++) {
		if(item == this->data[i]) { //is there a match?
			return i; //there was, so return the index
		}
	}

	//no match, so return -1
	return -1;
}

//serialization operator
std::ostream& operator<<(std::ostream& os, const ScoreList& ml) {
	os << ml.length() << '\n'; //serialize the length as a sentinel
	for(const int& s : ml.data) { //loop through the score vector
		os << s << '\n'; //and serialize each score
	}
	return os; //and return the stream
}

//deserialization operator
std::istream& operator>>(std::istream& is, ScoreList& ml) {
	int tempLen = 0; //used to read in the length
	is >> tempLen; //read in the saved length
	for(int i = 0; i < tempLen; i++) { //loop over the saved length
		int tempScore; //used to read in the saved scores
		is >> tempScore; //read in each saved score
		ml.push(tempScore); //and add the score to the list
	}
	return is; //and return the stream
}

//end of implementation
