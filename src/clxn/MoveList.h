/*
 * MoveList.h
 * Declares a class that represents a list of Othello moves
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <vector>
#include "../board/Move.h"
#include "../except/IndexException.h"

//class declaration
class MoveList final {
	//public fields and methods
	public:
		//constructor
		MoveList();

		//destructor
		~MoveList();

		//copy constructor
		MoveList(const MoveList& ml);

		//move constructor
		MoveList(MoveList&& ml);

		//assignment operator
		MoveList& operator=(const MoveList& src);

		//move operator
		MoveList& operator=(MoveList&& src);

		//subscript operator
		Move& operator[](int index);

		//getter methods
		
		//returns the length of the MoveList
		int length() const;

		//other methods
		
		//adds a Move to the list
		void push(const Move& m);

		//removes the top Move from the list and returns it
		Move pop();

		//removes a move at a specific index from the list
		//and returns it
		Move removeAt(int index);

		//clears the MoveList
		void clear();

		//returns whether a given move is in the list
		bool contains(const Move& m) const;

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os, 
						const MoveList& ml);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						MoveList& ml);

	//private fields and methods
	private:
		//field
		std::vector<Move> data; //the actual list data
};

//end of class
