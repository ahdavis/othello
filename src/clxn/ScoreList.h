/*
 * ScoreList.h
 * Declares a class that represents a list of Othello scores
 * Created on 11/20/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2017  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <vector>
#include <algorithm>
#include "../except/IndexException.h"

//class declaration
class ScoreList final {
	//public fields and methods
	public:
		//constructor
		ScoreList();

		//destructor
		~ScoreList();

		//copy constructor
		ScoreList(const ScoreList& sl);

		//move constructor
		ScoreList(ScoreList&& sl);

		//assignment operator
		ScoreList& operator=(const ScoreList& src);

		//move operator
		ScoreList& operator=(ScoreList&& src);

		//subscript operator
		int& operator[](int index);

		//getter methods
		
		//returns the length of the ScoreList
		int length() const;

		//other methods
		
		//adds a score to the list
		void push(int s);

		//removes the top score from the list and returns it
		int pop();

		//removes a score at a specific index from the list
		//and returns it
		int removeAt(int index);

		//clears the ScoreList
		void clear();

		//returns the index of the maximum score in the list
		//returns -1 if the list is empty
		int maxIndex() const;

		//returns the index of the minimum score in the list
		//returns -1 if the list is empty
		int minIndex() const;

		//returns the index of a given item in the list
		//returns -1 if the item is not found
		int indexOf(int item) const;

		//serialization operator
		friend std::ostream& operator<<(std::ostream& os, 
						const ScoreList& sl);

		//deserialization operator
		friend std::istream& operator>>(std::istream& is,
						ScoreList& sl);

	//private fields and methods
	private:
		//field
		std::vector<int> data; //the actual list data
};

//end of class
